#include <GL/glut.h>
#include <cstdlib>
#include <thread>
#include <vector>
#include <random>
#include "Snowflake.h"
#include "Point.h"
#include "Matrix.h"
#include "Cloud.h"

using namespace std;


static GLuint snowflakeTexture, cloudTexture;
static long generationFrequencyMilliseconds = 100;
static vector<Snowflake> flakes;

chrono::high_resolution_clock::time_point& getLastGeneratedFlake(){
    static chrono::high_resolution_clock::time_point lastGeneratedFlake;
    return lastGeneratedFlake;
}

Cloud& getCloud(){
    static Cloud c(0, 0, 0, 4);
    return c;
}

Point& getUp(){
    static Point up(0, 1, 0);
    return up;
}

Point& getCenter(){
    static Point center(0, 0, 0);
    return center;
}

Point& getEye(){
    static Point eye(0, 0, 10);
    return eye;
}

random_device& getRandomDevice(){
    static random_device rd;
    return rd;
}

mt19937_64& getEngine()
{
    static mt19937_64 engine(getRandomDevice()());
    return engine;
}

std::uniform_real_distribution<double>& getRandom_11()
{
    static std::uniform_real_distribution<double> random_11(-1, 1);
    return random_11;
}

void idle() {
    std::this_thread::sleep_for(std::chrono::milliseconds(5));
    auto now = chrono::high_resolution_clock::now();
    if(chrono::duration_cast<chrono::milliseconds>(now - getLastGeneratedFlake()).count() >= generationFrequencyMilliseconds){
        position cPos = getCloud().getCurrentPosition();
        flakes.emplace_back( getRandom_11()(getEngine()) * 0.8 + cPos.x, getRandom_11()(getEngine()) * 0.6 - 0.4 + cPos.y, cPos.z);
        getLastGeneratedFlake() = chrono::high_resolution_clock::now();
    }
    glutPostRedisplay();
}

GLuint LoadTexture(const char * filename, int w, int h, bool alpha, int offset)
{
    GLuint texture;
    int width, height;
    unsigned char * data;

    FILE * file;
    file = fopen( filename, "rb" );

    if ( file == nullptr ) return 0;
    width = w;
    height = h;
    data = (unsigned char *)malloc( width * height * (alpha ? 4 : 3) );
    fseek(file, offset, SEEK_SET);
    fread( data, width * height * (alpha ? 4 : 3), 1, file );
    fclose( file );

    for(int i = 0; i < width * height ; ++i)
    {
        int index = i*(alpha ? 4 : 3);
        if(!alpha){
            unsigned char B,R;
            B = data[index];
            R = data[index+2];

            data[index] = R;
            data[index+2] = B;
        } else {
            unsigned char R, G, B, A;
            A = data[index + 3];
            R = data[index + 2];
            G = data[index + 1];
            B = data[index];

            data[index] = R;
            data[index + 1] = G;
            data[index + 2] = B;
            data[index + 3] = A;
        }
    }

    glGenTextures( 1, &texture );
    glBindTexture( GL_TEXTURE_2D, texture );
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE,GL_MODULATE );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST );

    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT );
    gluBuild2DMipmaps( GL_TEXTURE_2D, (alpha ? GL_RGBA : GL_RGB), width, height,alpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, data );
    free( data );

    return texture;
}

void drawElement(GLuint element, Matrix rotationMatrix, double scalingFactor){
    glBindTexture(GL_TEXTURE_2D, element);
    glBegin(GL_QUADS);
    Point p1 = rotationMatrix.product1(Point(-0.5 * scalingFactor, -0.5 * scalingFactor, 0));
    Point p2 = rotationMatrix.product1(Point(+0.5 * scalingFactor, -0.5 * scalingFactor, 0));
    Point p3 = rotationMatrix.product1(Point(+0.5 * scalingFactor, +0.5 * scalingFactor, 0));
    Point p4 = rotationMatrix.product1(Point(-0.5 * scalingFactor, +0.5 * scalingFactor, 0));
    glTexCoord3d(0, 0, 0);         glVertex3d(p1.getX(), p1.getY(), p1.getZ());
    glTexCoord3d(1, 0, 0);         glVertex3d(p2.getX(), p2.getY(), p2.getZ());
    glTexCoord3d(1, 1, 0);         glVertex3d(p3.getX(), p3.getY(), p3.getZ());
    glTexCoord3d(0, 1, 0);         glVertex3d(p4.getX(), p4.getY(), p4.getZ());
    glEnd();
}

// Fixes up camera and remaps texture when window reshaped.
void reshape(int width, int height) {
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(80, GLfloat(width)/(float)height, 1, 40);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(getEye().getX(), getEye().getY(),  getEye().getZ(),
              getCenter().getX(), getCenter().getY(), getCenter().getZ(),
              getUp().getX(), getUp().getY(), getUp().getZ());
    glEnable(GL_TEXTURE_2D);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glBindTexture(GL_TEXTURE_2D, snowflakeTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}

void display() {
    glClear(GL_COLOR_BUFFER_BIT);

    Point look = Point::diff(getEye(), getCenter());
    look.normalize();

    Point right = Point::vProduct(getUp(), look);
    right.normalize();

    Point up2 = Point::vProduct(look, right);
    up2.normalize();

    Matrix rotationMatrix(right.getX(), right.getY(), right.getZ(),
                          up2.getX(), up2.getY(), up2.getZ(),
                          look.getX(), look.getY(), look.getZ());

    position cPos = getCloud().getCurrentPosition();

    for(auto it = flakes.begin(); it != flakes.end(); it++){
        glPushMatrix();
        position pos = it->getCurrentPosition();

        glTranslated(pos.x, pos.y, pos.z);
        if(pos.dead){
            flakes.erase(it);
        }
        drawElement(snowflakeTexture, rotationMatrix, 0.25);
        glPopMatrix();
    }

    glPushMatrix();
    glTranslated(cPos.x, cPos.y, cPos.z);
    drawElement(cloudTexture, rotationMatrix, 2);
    glPopMatrix();

    glFlush();
}

// Initializes GLUT and enters the main loop.
int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(1280, 720);
    glutCreateWindow("Particle system");
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutIdleFunc(idle);
    snowflakeTexture = LoadTexture("../data/snowflake4.bmp", 256, 256, false, 0);
    cloudTexture = LoadTexture("../data/cloud2.bmp", 256, 256, false, 0);
    glutMainLoop();
}