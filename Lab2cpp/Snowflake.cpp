//
// Created by ivan on 12/14/21.
//

#include "Snowflake.h"

using namespace std;

Snowflake::Snowflake(double xStart, double yStart, double zStart): xStart(xStart), yStart(yStart), zStart(zStart){
    this->startTimestamp = std::chrono::high_resolution_clock::now();
}

position Snowflake::getCurrentPosition() {
    auto age = chrono::duration_cast<chrono::milliseconds>(chrono::high_resolution_clock::now() - startTimestamp).count();
    position ret(xStart, yStart - (double) maxDistance * (double) age / (double) lifetime.count(), zStart, age > lifetime.count());
    return ret;
}


