//
// Created by ivan on 12/14/21.
//

#ifndef LAB2CPP_POINT_H
#define LAB2CPP_POINT_H

#include <iostream>

class Point {
private:
    double x, y, z;
public:
    Point()= default;
    Point(double x, double y, double z);
    void normalize();
    [[nodiscard]] double norm() const;
    friend std::ostream& operator<<(std::ostream& os, const Point& p);
    static double sProduct(const Point& p1, const Point& p2);
    static Point vProduct(const Point& p1, const Point& p2);
    static double cosine(const Point& p1, const Point& p2);

    [[maybe_unused]] static double angleDegrees(const Point& p1, const Point& p2);
    static Point diff(const Point& p1, const Point& p2);
    [[nodiscard]] double getX() const;
    [[nodiscard]] double getY() const;
    [[nodiscard]] double getZ() const;
};


#endif //LAB2CPP_POINT_H
