//
// Created by ivan on 12/14/21.
//

#ifndef LAB2CPP_SNOWFLAKE_H
#define LAB2CPP_SNOWFLAKE_H

#include <chrono>
#include "Position.h"


class Snowflake {
private:
    double xStart{}, yStart{}, zStart{};
    std::chrono::high_resolution_clock::time_point startTimestamp;
    constexpr const static std::chrono::milliseconds lifetime = std::chrono::milliseconds (2000);
    constexpr const static double maxDistance = 4;
public:
    Snowflake()= default;
    Snowflake(double xStart, double yStart, double zStart);
    position getCurrentPosition();
};


#endif //LAB2CPP_SNOWFLAKE_H
