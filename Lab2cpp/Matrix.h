//
// Created by ivan on 12/14/21.
//

#ifndef LAB2CPP_MATRIX_H
#define LAB2CPP_MATRIX_H


#include "Point.h"

class Matrix {
private:
    double m[3][3]{};
public:
    Matrix(double r1, double r2, double r3,
            double u1, double u2, double u3,
            double l1, double l2, double l3);
    Point product1(Point point);
};


#endif //LAB2CPP_MATRIX_H
