//
// Created by ivan on 12/14/21.
//

#ifndef LAB2CPP_POSITION_H
#define LAB2CPP_POSITION_H

struct position{
    double x, y, z;
    bool dead;
    position()= default;
    position(double x, double y, double z, bool d) : x(x), y(y), z(z), dead(d){}
};

#endif //LAB2CPP_POSITION_H
