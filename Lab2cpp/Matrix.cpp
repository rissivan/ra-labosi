//
// Created by ivan on 12/14/21.
//

#include "Matrix.h"

Matrix::Matrix(double r1, double r2, double r3, double u1, double u2, double u3, double l1, double l2, double l3) {
    m[0][0] = r1;
    m[0][1] = u1;
    m[0][2] = l1;
    m[1][0] = r2;
    m[1][1] = u2;
    m[1][2] = l2;
    m[2][0] = r3;
    m[2][1] = u3;
    m[2][2] = l3;
}

Point Matrix::product1(Point point) {
    double px = m[0][0] * point.getX() + m[0][1] * point.getY() + m[0][2] * point.getZ();
    double py = m[1][0] * point.getX() + m[1][1] * point.getY() + m[1][2] * point.getZ();
    double pz = m[2][0] * point.getX() + m[2][1] * point.getY() + m[2][2] * point.getZ();
    return {px, py, pz};
}
