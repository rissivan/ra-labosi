cmake_minimum_required(VERSION 3.22)
project(Lab2cpp)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS -lGL)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -lglut")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -lGLU")

add_executable(Lab2cpp main.cpp Snowflake.cpp Snowflake.h Point.cpp Point.h Matrix.cpp Matrix.h Cloud.cpp Cloud.h Position.h)
