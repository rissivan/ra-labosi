//
// Created by ivan on 12/14/21.
//

#include <cmath>
#include "Cloud.h"

position Cloud::getCurrentPosition() {
    auto now = std::chrono::high_resolution_clock::now();
    auto diff = now - startTimeStamp;
    if(diff > period){
        startTimeStamp += period;
        diff -= period;
    }
    double angle = ((double)diff.count() / period.count()) * 2 * M_PI;
    position ret(centerX + r * sin(angle), centerY, centerZ + r * cos(angle), false);
    return ret;
}

Cloud::Cloud(double centerX, double centerY, double centerZ, double r): centerX(centerX), centerY(centerY), centerZ(centerZ), r(r) {
    this->startTimeStamp = std::chrono::high_resolution_clock::now();
}
