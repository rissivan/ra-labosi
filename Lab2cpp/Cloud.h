//
// Created by ivan on 12/14/21.
//

#ifndef LAB2CPP_CLOUD_H
#define LAB2CPP_CLOUD_H

#include <chrono>
#include "Position.h"

class Cloud {
    double centerX{}, centerY{}, centerZ{}, r{};
    std::chrono::high_resolution_clock::time_point startTimeStamp;
    constexpr static const std::chrono::high_resolution_clock::duration period = std::chrono::milliseconds(20000);
public:
    Cloud()= default;
    Cloud(double centerX, double centerY, double centerZ, double r);
    position getCurrentPosition();
};


#endif //LAB2CPP_CLOUD_H
