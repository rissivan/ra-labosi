//
// Created by ivan on 12/14/21.
//

#include "Point.h"
#include <cmath>

Point::Point(double x, double y, double z): x(x), y(y), z(z) {}

double Point::norm() const {
    return sqrt(x * x + y * y + z * z);
}

void Point::normalize() {
    double len = norm();
    x /= len;
    y /= len;
    z /= len;
}

std::ostream &operator<<(std::ostream &os, const Point &p){
    os << "(" << p.x << ", " << p.y << ", " << p.z << ")";
    return os;
}

double Point::sProduct(const Point &p1, const Point &p2) {
    return p1.x * p2.x + p1.y + p2.y + p1.z * p2.z;
}

Point Point::vProduct(const Point &p1, const Point &p2) {
    return {p1.y * p2.z - p1.z * p2.y, p1.z * p2.x - p1.x * p2.z, p1.x * p2.y - p1.y * p2.x};
}

double Point::cosine(const Point &p1, const Point &p2) {
    double dot = sProduct(p1, p2);
    return dot / (p1.norm() * p2.norm());
}

Point Point::diff(const Point &p1, const Point &p2) {
    return {p1.x - p2.x, p1.y - p2.y, p1.z - p2.z};
}

double Point::getX() const {
    return x;
}

double Point::getY() const {
    return y;
}

double Point::getZ() const {
    return z;
}

[[maybe_unused]] double Point::angleDegrees(const Point &p1, const Point &p2) {
    double cos = cosine(p1, p2);
    double angleRad = acos(cos);
    return angleRad / M_PI * 180;
}
