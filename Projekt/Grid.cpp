//
// Created by ivan on 1/12/22.
//

#include <iostream>
#include "Grid.h"
#include "Elements/EmptyElement.h"

void Grid::step() {
    for(int i = 0; i < WIDTH; i++){
        for(int j = HEIGHT - 1; j >= 0; j--){
            if(!elements[i][j]->isEmpty()){
                elements[i][j]->step();
            }
        }
    }
}

Grid::~Grid() {
    for(int i = 0; i < WIDTH; i++){
        for(int j = 0; j < HEIGHT; j++){
            if(elements[i][j]){
                delete elements[i][j];
            }
        }
    }
}

Grid::Grid(){
    std::fill(elements.begin(), elements.end(), std::array<Element*, HEIGHT>());
    for(int i = 0; i < WIDTH; i++){
        for(int j = 0; j < HEIGHT; j++){
            elements[i][j] = new EmptyElement(i, j, *this);
        }
    }
}

void Grid::moveElement(Element *element, int oldX, int oldY) {
    if(element->x < 0 || element->y < 0 || element->x >= WIDTH || element->y >= HEIGHT){
        delete element;
        elements[oldX][oldY] = new EmptyElement(oldX, oldY, *this);
    } else {
        auto* tmp = elements[element->x][element->y];
        elements[element->x][element->y] = element;
        elements[oldX][oldY] = tmp;
    }
}


