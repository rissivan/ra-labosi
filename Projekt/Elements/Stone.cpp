//
// Created by ivan on 1/12/22.
//

#include "Stone.h"

Stone::Stone(int x, int y, Grid& grid): Solid(x, y, true, false, grid) {
}

Stone::~Stone() = default;

void Stone::step() {
}

std::tuple<int, int, int> Stone::getColour() {
    return stoneColour;
}

