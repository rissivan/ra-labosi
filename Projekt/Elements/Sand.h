//
// Created by ivan on 1/12/22.
//

#ifndef PROJEKT_SAND_H
#define PROJEKT_SAND_H


#include "Solid.h"

class Sand: public Solid {
    int verticalVelocity = 1;
    int horizontalVelocity = 0;
    constexpr static const double FLAT_SURFACE_ENERGY_TRANSFER_RATE = 0.35;
    constexpr static const double SAND_ENERGY_TRANSFER_RATE = 0.25;
    constexpr static const double FRICTION_RATE = 0.5;
public:
    Sand(int x, int y, Grid& grid);
    ~Sand() override;
    void step() override;
    std::tuple<int, int, int> getColour() override;
};


#endif //PROJEKT_SAND_H
