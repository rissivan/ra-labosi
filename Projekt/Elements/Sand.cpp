//
// Created by ivan on 1/12/22.
//

#include <iostream>
#include "Sand.h"

Sand::Sand(int x, int y, Grid &grid): Solid(x, y, false, true, grid){
}

Sand::~Sand() = default;

void Sand::step() {
    if(stationary)
        return;
    int oldX = x;
    int oldY = y;
    int offsetX;
    int offsetY;

    bool outOfBounds = false;

    //move right
    if(horizontalVelocity > 0){
        for(offsetX = 1; offsetX <= horizontalVelocity; offsetX++){
            if(x + offsetX >= WIDTH){
                x += offsetX;
                outOfBounds = true;
                break;
            }
            if(!grid.elements[x + offsetX][y]->isEmpty()){
                x += (offsetX - 1);
                break;
            }
            if(offsetX == horizontalVelocity){
                x += offsetX;
            }
        }
    //move left
    } else if (horizontalVelocity < 0){
        for(offsetX = -1; offsetX >= horizontalVelocity; offsetX--){
            if(x + offsetX < 0 ){
                x += offsetX;
                outOfBounds = true;
                break;
            }
            if(!grid.elements[x + offsetX][y]->isEmpty()){
                x += (offsetX + 1);
                break;
            }
            if(offsetX == horizontalVelocity){
                x += offsetX;
            }
        }
    }
    //downwards
    if(!outOfBounds){
        for(offsetY = 1; offsetY <= verticalVelocity; offsetY++){
            if(oldY + offsetY >= HEIGHT){
                y = oldY + offsetY;
                break;
            }
            //move downwards if empty
            if(grid.elements[x][oldY + offsetY]->isEmpty()){
                y++;
            } else {
                //if the particle hits a movable object(sand) and can move either left or right, randomly pick the side
                if(x + 1 < WIDTH && grid.elements[x + 1][oldY + offsetY]->isEmpty() && grid.elements[x + 1][oldY + offsetY - 1]->isMovable() &&
                        x - 1 >= 0 && grid.elements[x - 1][oldY + offsetY]->isEmpty() && grid.elements[x - 1][oldY + offsetY - 1]->isMovable()){
                    y++;
                    if(getBool()){
                        horizontalVelocity += (int) (SAND_ENERGY_TRANSFER_RATE * (double) verticalVelocity); // when hitting sand, go right upon contact
                        x++;
                    } else {
                        horizontalVelocity += -(int) (SAND_ENERGY_TRANSFER_RATE * (double) verticalVelocity); // when hitting sand, go left upon contact
                        x--;
                    }
                //otherwise, if the particle hits a movable object(sand) and can only move right, go right
                } else if(x + 1 < WIDTH && grid.elements[x + 1][oldY + offsetY]->isEmpty() && grid.elements[x + 1][oldY + offsetY - 1]->isMovable()){
                    horizontalVelocity += (int) (SAND_ENERGY_TRANSFER_RATE * (double) verticalVelocity); // when hitting sand
                    x++;
                    y++;
                //otherwise, if the particle hits a movable object(sand) and can only move left, go left
                } else if (x - 1 >= 0 && grid.elements[x - 1][oldY + offsetY]->isEmpty() && grid.elements[x - 1][oldY + offsetY - 1]->isMovable()){
                    horizontalVelocity += -(int) (SAND_ENERGY_TRANSFER_RATE * (double) verticalVelocity); // when hitting sand
                    x--;
                    y++;
                } else {
                    //otherwise, if the particle hits an immovable object(stone)
                    if(!grid.elements[x][oldY + offsetY]->isMovable() || grid.elements[x][oldY + offsetY]->isStationary()){
                        y = oldY + offsetY - 1;

                        //if the particle can go either way, choose it randomly
                        if(x + 1 < WIDTH && grid.elements[x + 1][oldY + offsetY - 1]->isEmpty() &&
                           x - 1 >= 0 && grid.elements[x - 1][oldY + offsetY - 1]->isEmpty()){
                            horizontalVelocity += (getBool() ? 1 : -1) * (int)(FLAT_SURFACE_ENERGY_TRANSFER_RATE * (double) verticalVelocity);
                        //otherwise, if the particle can only go right, go right
                        }else if(x + 1 < WIDTH && grid.elements[x + 1][oldY + offsetY - 1]->isEmpty()){
                            horizontalVelocity += (int)(FLAT_SURFACE_ENERGY_TRANSFER_RATE * (double) verticalVelocity);  //when hitting flat surface
                        //otherwise, if the particle can only go left, go left
                        } else if (x - 1 >= 0 && grid.elements[x - 1][oldY + offsetY - 1]->isEmpty()){
                            horizontalVelocity += -(int)(FLAT_SURFACE_ENERGY_TRANSFER_RATE * (double) verticalVelocity); //when hitting flat surface
                        //otherwise, stop the particle
                        } else {
                            horizontalVelocity = 0;
                        }
                        if(!grid.elements[x][oldY + offsetY]->isMovable())
                            verticalVelocity = 0;
                        else
                            verticalVelocity /= 2;
                    }
                    break;
                }
            }
        }

        if(horizontalVelocity == 0 && verticalVelocity == 0 && !grid.elements[x][y + 1]->isEmpty()){
            stationary = true;
        }
        if(!this->stationary)
            verticalVelocity++;
        horizontalVelocity = (int)(FRICTION_RATE * horizontalVelocity);
    }

    if(x != oldX || y != oldY)
        grid.moveElement(this, oldX, oldY);
}

std::tuple<int, int, int> Sand::getColour() {
    return sandColour;
}

