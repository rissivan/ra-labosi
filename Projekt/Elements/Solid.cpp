//
// Created by ivan on 1/12/22.
//

#include "Solid.h"

Solid::Solid(int x, int y, bool stationary, bool movable, Grid &grid):
        Element(x, y, stationary, movable, false, false, grid){
}

Solid::~Solid() = default;
