//
// Created by ivan on 1/12/22.
//

#include "Element.h"

Element::Element(int x, int y, bool stationary, bool movable, bool empty, bool liquid, Grid& grid):
    x(x), y(y), stationary(stationary), movable(movable), empty(empty), liquid(liquid), grid(grid) {
}

void Element::step() {
}

std::tuple<int, int, int> Element::getColour() {
    return noColour;
}

bool Element::isEmpty() const {
    return empty;
}

[[maybe_unused]] bool Element::isMovable() const {
    return movable;
}

[[maybe_unused]] bool Element::isStationary() const {
    return stationary;
}

bool Element::isLiquid() const {
    return liquid;
}

Element::~Element() = default;
