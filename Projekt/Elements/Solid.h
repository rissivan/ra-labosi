//
// Created by ivan on 1/12/22.
//

#ifndef PROJEKT_SOLID_H
#define PROJEKT_SOLID_H


#include "Element.h"

class Solid: public Element {
protected:
    Solid(int x, int y, bool stationary, bool movable, Grid& grid);
    ~Solid() override;
};


#endif //PROJEKT_SOLID_H
