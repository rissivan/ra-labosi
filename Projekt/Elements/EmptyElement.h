//
// Created by ivan on 1/12/22.
//

#ifndef PROJEKT_EMPTYELEMENT_H
#define PROJEKT_EMPTYELEMENT_H


#include "Element.h"

class EmptyElement: public Element {
public:
    EmptyElement(int x, int y, Grid& grid);
    ~EmptyElement() override;
};


#endif //PROJEKT_EMPTYELEMENT_H
