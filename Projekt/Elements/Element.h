//
// Created by ivan on 1/12/22.
//

#ifndef PROJEKT_ELEMENT_H
#define PROJEKT_ELEMENT_H


#include <tuple>
#include "../Grid.h"

class Grid;

class Element {
protected:
    int x;
    int y;
    bool movable;
    bool empty;
    bool stationary;
    bool liquid;
    Grid& grid;
    Element(int x, int y, bool stationary, bool movable, bool empty, bool liquid, Grid& grid);
public:
    virtual ~Element();
    virtual void step();
    virtual std::tuple<int, int, int> getColour();
    [[nodiscard]] bool isEmpty() const;
    [[maybe_unused]] bool isMovable() const;
    [[maybe_unused]] bool isStationary() const;
    bool isLiquid() const;

    friend class Grid;
};


#endif //PROJEKT_ELEMENT_H
