//
// Created by ivan on 1/12/22.
//

#ifndef PROJEKT_STONE_H
#define PROJEKT_STONE_H


#include "Solid.h"

class Stone: public Solid {
public:
    Stone(int x, int y, Grid& grid);
    ~Stone() override;
    void step() override;
    std::tuple<int, int, int> getColour() override;
};


#endif //PROJEKT_STONE_H
