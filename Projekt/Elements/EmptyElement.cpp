//
// Created by ivan on 1/12/22.
//

#include "EmptyElement.h"

EmptyElement::EmptyElement(int x, int y, Grid& grid): Element(x, y, false, true, true, false, grid){
}

EmptyElement::~EmptyElement() = default;
