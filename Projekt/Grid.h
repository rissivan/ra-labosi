//
// Created by ivan on 1/12/22.
//

#ifndef PROJEKT_GRID_H
#define PROJEKT_GRID_H

#include <array>
#include "Elements/Element.h"
#include "Util/Util.h"

class Element;

class Grid {
public:
    std::array<std::array<Element*, HEIGHT>, WIDTH> elements{};
    Grid();
    ~Grid();
    void step();
    void moveElement(Element* element, int oldX, int oldY);

};


#endif //PROJEKT_GRID_H
