//
// Created by ivan on 1/12/22.
//
#ifndef PROJEKT_UTIL_H
#define PROJEKT_UTIL_H

#include <tuple>
#include <random>

constexpr const static int WIDTH = 1280;
constexpr const static int HEIGHT = 720;

constexpr const static std::tuple<int, int, int> noColour(0, 0, 0);
constexpr const static std::tuple<int, int, int> stoneColour(128, 128, 128);
constexpr const static std::tuple<int, int, int> sandColour(128, 128, 0);

std::random_device& getRandomDevice();
std::mt19937_64& getEngine();
std::uniform_real_distribution<double>& getDist();
double getDouble();
bool getBool();

#endif //PROJEKT_UTIL_H
