//
// Created by ivan on 1/13/22.
//

#include "Util.h"

std::random_device& getRandomDevice(){
    static std::random_device rd;
    return rd;
}

std::mt19937_64& getEngine(){
    static std::mt19937_64 engine(getRandomDevice()());
    return engine;
}

std::uniform_real_distribution<double>& getDist(){
    static std::uniform_real_distribution<double> dist(0, 1);
    return dist;
}

double getDouble(){
    return getDist()(getEngine());
}

bool getBool(){
    return getDouble() >= 0.5;
}
