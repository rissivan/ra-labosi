#include <iostream>
#include <GL/glut.h>
#include <cstdlib>
#include <thread>
#include "Grid.h"
#include "Elements/Stone.h"
#include "Elements/Sand.h"


using namespace std;

int cursorSize = 1;
static pair<int, int> lastCursorCoordinates(-1, -1);
static pair<int, int> cursorCoordinates(0, 0);
bool mouseClicked = false;
bool lineOn = false;

Grid grid;

enum elements{STONE, SAND, PLACEHOLDER};
elements currentElement = STONE;

void drawGrid(){
    glBegin(GL_POINTS);

    for(int i = 0; i < WIDTH; i++){
        for(int j = 0; j < HEIGHT; j++){
            auto colour = grid.elements[i][j]->getColour();
            glColor3ub(get<0>(colour), get<1>(colour), get<2>(colour));
            glVertex2i(i, j);
        }
    }
    glEnd();
}

void drawCursor(){
    int x = cursorCoordinates.first;
    int y = cursorCoordinates.second;
    glColor3d(1, 1, 1);
    glBegin(GL_LINE_STRIP);
    glVertex2i(x - cursorSize, y - cursorSize);
    glVertex2i(x - cursorSize, y + cursorSize);
    glVertex2i(x + cursorSize, y + cursorSize);
    glVertex2i(x + cursorSize, y - cursorSize);
    glVertex2i(x - cursorSize, y - cursorSize);
    glEnd();
}

void drawCorner(){
    tuple<int, int, int> colour;
    if(lineOn){
        colour = noColour;
    } else if(currentElement == elements::SAND){
        colour = sandColour;
    } else if (currentElement == elements::STONE){
        colour = stoneColour;
    }

    glColor3ub(get<0>(colour), get<1>(colour), get<2>(colour));
    glBegin(GL_QUADS);
    glVertex2i(WIDTH - 1, 0);
    glVertex2i(WIDTH - 41, 0);
    glVertex2i(WIDTH - 41, 40);
    glVertex2i(WIDTH - 1, 40);
    glEnd();
}

void createElements(int x, int y){
    for(int i = x - cursorSize + 1; i < x + cursorSize; i++){
        if(i < 0 || i >= WIDTH)
            continue;
        for(int j = y + cursorSize - 1; j >= y - cursorSize + 1; j--){
            if(j < 0 || j >= HEIGHT)
                continue;
            if(grid.elements[i][j]->isEmpty()){
                if(currentElement == elements::STONE){
                    delete grid.elements[i][j];
                    grid.elements[i][j] = new Stone(i, j, grid);
                } else if (currentElement == elements::SAND){
                    delete grid.elements[i][j];
                    grid.elements[i][j] = new Sand(i, j, grid);
                }
            }
        }
    }
}

void createElements(){
    createElements(cursorCoordinates.first, cursorCoordinates.second);
}


void display() {
    glClear(GL_COLOR_BUFFER_BIT);
    if(mouseClicked){
        createElements();
    }
    grid.step();
    drawGrid();
    drawCorner();
    drawCursor();
    glFlush();
}

void idle() {
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    glutPostRedisplay();
}

void mouseFunc(int button, int state, int x, int y){
    if(!lineOn) {
        if(state == 0){
            mouseClicked = true;
            lastCursorCoordinates.first = x;
            lastCursorCoordinates.second = y;
        } else {
            mouseClicked = false;
            lastCursorCoordinates.first = -1;
            lastCursorCoordinates.second = -1;
        }
        if(button == 0 && state == 0){
            createElements();
        }
    } else {
        for(int i = 0; i < WIDTH; i++){
            delete grid.elements[i][cursorCoordinates.second];
            grid.elements[i][cursorCoordinates.second] = new Stone(i, cursorCoordinates.second, grid);
        }
    }
}

void passiveMotionFunc(int x, int y){
    cursorCoordinates.first = x;
    cursorCoordinates.second = y;
}

void createElementsOnTheLine(){
    int x1 = lastCursorCoordinates.first;
    int y1 = lastCursorCoordinates.second;
    int x2 = cursorCoordinates.first;
    int y2 = cursorCoordinates.second;

    if(abs(x1 - x2) > abs(y1 - y2)){
        double dy = ((double) (y2 - y1)) / ((double) (x2 - x1));
        if(x1 < x2){
            for(int i = 0; i <= x2 - x1; i++){
                createElements(x1 + i, y1 + static_cast<int>(lround(i * dy)));
            }
        } else {
            for(int i = 0; i <= x1 - x2; i++){
                createElements(x1 - i, y1 - static_cast<int>(lround(i * dy)));
            }
        }
    } else {
        double dx = ((double) (x2 - x1)) / ((double) (y2 - y1));
        if(y1 < y2){
            for(int i = 0; i <= y2 - y1; i++){
                createElements(x1 + static_cast<int>(lround(i * dx)), y1 + i);
            }
        } else {
            for(int i = 0; i <= y1 - y2; i++){
                createElements(x1 - static_cast<int>(lround(i * dx)), y1 - i);
            }
        }
    }
}

void motionFunc(int x, int y){
    if(!lineOn) {
        cursorCoordinates.first = x;
        cursorCoordinates.second = y;
        createElementsOnTheLine();
        lastCursorCoordinates.first = x;
        lastCursorCoordinates.second = y;
    }
}

void keyboardFunc(unsigned char key, int x, int y){
    if(key == '+'){
        if(cursorSize < 15){
            cursorSize++;
        }
    } else if (key == '-'){
        if(cursorSize > 1){
            cursorSize--;
        }
    } else if (key == 's'){
        currentElement = static_cast<elements> (currentElement + 1);
        if(currentElement == elements::PLACEHOLDER){
            currentElement = static_cast<elements> (0);
        }
    } else if (key == 'l') {
        lineOn = !lineOn;
    } else if ((int) key == 27){
        glutDestroyWindow(glutGetWindow());
        exit(0);
    }
}

void reshape(int width, int height) {
    glDisable(GL_DEPTH_TEST);
    glViewport(0, 0, 1280, 720);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, 1280 - 1, 720 - 1, 0, 0, 1);
    glMatrixMode(GL_MODELVIEW);
    glutReshapeWindow(1280, 720);
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(1280, 720);
    glutCreateWindow("Sand");
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutIdleFunc(idle);
    glutMouseFunc(mouseFunc);
    glutMotionFunc(motionFunc);
    glutPassiveMotionFunc(passiveMotionFunc);
    glutKeyboardFunc(keyboardFunc);
    glutMainLoop();
}
