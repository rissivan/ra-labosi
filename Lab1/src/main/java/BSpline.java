import structures.linalg.matrix.IMatrix;
import structures.linalg.matrix.Matrix;
import structures.linalg.util.IncompatibleOperandException;
import structures.linalg.util.ReadOnlyException;
import structures.linalg.vector.IVector;
import structures.linalg.vector.Vector;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BSpline {

    //kasnije probati zapisati R ko SubMatrix

    private IMatrix B;
    private IMatrix dB;
    private List<IVector> R;
    private List<IMatrix> B_R;
    private List<IMatrix> dB_R;
    private List<IVector> points;
    private List<IVector> tangents;

    private int noSegments;

    private static final int NO_POINTS_PER_SEGMENT = 60;

    public BSpline(String path){
        parseFile(path);
        calculateSegmentMatrices();
        sampleSplinePoints(NO_POINTS_PER_SEGMENT);
    }

    private void parseFile(String path){
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(path)))){
            double coefficient;

            String line = skipComments(bufferedReader);
            if(line.contains("/")){
                String[] frac = line.split("/");
                coefficient = Double.parseDouble(frac[0]) / Double.parseDouble(frac[1]);
            } else {
                coefficient = Double.parseDouble(line);
            }


            List<double[]> BValues = new ArrayList<>();
            while(true){
                line = skipComments(bufferedReader);
                if(line.length() == 0)
                    break;
                double[] row = Arrays.stream(line.split(" ")).mapToDouble(Double::parseDouble).toArray();
                BValues.add(row);
            }

            int rows = BValues.size();
            int cols = BValues.get(0).length;
            this.B = new Matrix(rows, cols);
            for(int i = 0; i < rows; i++){
                double[] row = BValues.get(i);
                for(int j = 0; j < cols; j++){
                    B.set(i, j,coefficient * row[j]);
                }
            }

            this.dB = new Matrix(rows - 1, cols);
            for(int i = 1; i < rows; i++){
                double[] row = BValues.get(rows - i - 1);
                for(int j = 0; j < cols; j++){
                    dB.set(rows - i - 1, j, coefficient * i * row[j]);
                }
            }


            R = new ArrayList<>();
            while(true){
                line = skipComments(bufferedReader);
                if(line.equals("END"))
                    break;
                double[] row = Arrays.stream(line.split(" ")).mapToDouble(Double::parseDouble).toArray();
                R.add(new Vector(row));
            }



        } catch (IOException | ReadOnlyException e) {
            e.printStackTrace();
        }
    }

    private static String skipComments(BufferedReader br) throws IOException {
        String line;
        do{
            line = br.readLine();
            if (line.length() == 0)
                break;
        } while (line.startsWith("#"));
        return line;
    }

    private void calculateSegmentMatrices(){
        this.B_R = new ArrayList<>();
        this.dB_R = new ArrayList<>();

        this.noSegments = R.size() - 3;
        int rows = B.getRowsCount();
        int cols = R.get(0).getDimension();
        try{
            for(int i = 0; i < noSegments; i++){
                List<IVector> rList = R.subList(i, i + rows);
                IMatrix rMatrix = new Matrix(rows, cols);
                for(int j = 0; j < rows; j++){
                    for(int k = 0; k < cols; k++){
                        rMatrix.set(j, k, rList.get(j).get(k));
                    }
                }
                B_R.add(B.nMultiply(rMatrix));
                dB_R.add(dB.nMultiply(rMatrix));
            }
        } catch (ReadOnlyException | IncompatibleOperandException e) {
            e.printStackTrace();
        }
    }

    public void sampleSplinePoints(int noPointsPerSegment){
        try {
            points = new ArrayList<>();
            tangents = new ArrayList<>();

            for(int i = 0; i < noSegments; i++){
                for(int j = 0; j < noPointsPerSegment; j++){
                    double t = (double) j / (noPointsPerSegment - 1);
                    int Trows = B.getRowsCount();
                    IMatrix T = new Matrix(1, Trows);
                    IMatrix dT = new Matrix(1, Trows - 1);
                    for(int k = 0; k < Trows; k++){
                        double v = 1;
                        for(int l = 0; l < k; l++){
                            v *= t;
                        }
                            T.set(0, Trows - 1 - k, v);
                            if(k < Trows - 1)
                                dT.set(0, Trows - 2 - k, v);
                    }
                    IMatrix point = T.nMultiply(B_R.get(i));
                    IMatrix tangent = dT.nMultiply(dB_R.get(i));

                    points.add(createVectorFrom1DMatrix(point));
//                    if(j == 0 && i != 0 || j == noPointsPerSegment - 1)
//                        tangents.add(tangents.get(tangents.size() - 1));
//                    else
                        tangents.add(createVectorFrom1DMatrix(tangent));
                }
            }
        } catch (ReadOnlyException | IncompatibleOperandException e) {
            e.printStackTrace();
        }
    }

    private IVector createVectorFrom1DMatrix(IMatrix m){
        int size = m.getColsCount();
        double[] elements = new double[size];
        for(int i = 0; i < size; i++){
            elements[i] = m.get(0, i);
        }
        return new Vector(elements);
    }

    public List<IVector> getPoints() {
        return points;
    }

    public List<IVector> getTangents() {
        return tangents;
    }
}
