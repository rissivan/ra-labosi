package structures.objectStructures;

public class Triangle {
    int p1, p2, p3;

    private double a, b, c, d;

    private boolean visible;

    public Triangle(int p1, int p2, int p3){
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    public void setPlane(Plane plane){
        this.a = plane.getA();
        this.b = plane.getB();
        this.c = plane.getC();
        this.d = plane.getD();
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    public double getD() {
        return d;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public int getP1() {
        return p1;
    }

    public int getP2() {
        return p2;
    }

    public int getP3() {
        return p3;
    }

    @Override
    public String toString() {
        return "[" + p1 + "-" + p2 + "-" + p3 + "]";
    }
}
