package structures.objectStructures;

public class Plane {

    private double a, b, c, d;

    public Plane(Point3D p1, Point3D p2, Point3D p3){
        this.a = (p2.y - p1.y) * (p3.z - p1.z) - (p2.z - p1.z) * (p3.y - p1.y);
        this.b = -(p2.x - p1.x) * (p3.z - p1.z) + (p2.z - p1.z) * (p3.x - p1.x);
        this.c = (p2.x - p1.x) * (p3.y - p1.y) - (p2.y - p1.y) * (p3.x - p1.x);
        this.d = -p1.x * a - p1.y * b - p1.z * c;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    public double getD() {
        return d;
    }

    @Override
    public String toString() {
        return a + "x" + ((b < 0) ? "" : "+") + b + "y" + ((c < 0) ? "" : "+") + c + "z" + ((d < 0) ? "" : "+") + d + "=0";
    }
}
