package structures.objectStructures;

import structures.linalg.util.IncompatibleOperandException;
import structures.linalg.util.ReadOnlyException;
import structures.linalg.vector.IVector;
import structures.linalg.vector.Vector;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Object3D {
    private List<Point3D> vertices;
    private List<Triangle> triangles;
    private List<Plane> planes;
    private int noVertices;
    private int noPolygons;

    private double xMin;
    private double xMax;
    private double yMin;
    private double yMax;
    private double zMin;
    private double zMax;
    private double dx, dy, dz;
    private boolean minMaxSet = false;
    Point3D sTick;
    private boolean scaled = false;

    public Object3D(String path){
        this.vertices = new ArrayList<>();
        this.triangles = new ArrayList<>();
        this.planes = new ArrayList<>();
        loadData(path);
    }

    private void loadData(String path){
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(path)))){

            String line = "";
            while(line != null){
                line = reader.readLine();
                if(line == null)
                    break;
                else if (line.startsWith("v")){
                    noVertices++;
                    String[] coo = line.split(" ");
                    double x = Double.parseDouble(coo[1]);
                    double y = Double.parseDouble(coo[2]);
                    double z = Double.parseDouble(coo[3]);
                    Point3D vertex = new Point3D(x, y, z);
                    testMinMax(x, y, z);
                    vertices.add(vertex);
                }
                else if (line.startsWith("f")){
                    noPolygons++;
                    String[] vertices = line.split(" ");
                    triangles.add(new Triangle(Integer.parseInt(vertices[1]) - 1, Integer.parseInt(vertices[2]) - 1, Integer.parseInt(vertices[3]) - 1));
                }


            }

            dx = xMax - xMin;
            dy = yMax - yMin;
            dz = zMax - zMin;
            sTick = new Point3D((xMin + xMax) / 2, (yMin + yMax) / 2, (zMin + zMax) / 2);

            translatePoints();
            scalePoints(Math.max(dx, Math.max(dy, dz)) / 2);


            for(Triangle triangle: triangles){
                Point3D p1 = vertices.get(triangle.p1);
                Point3D p2 = vertices.get(triangle.p2);
                Point3D p3 = vertices.get(triangle.p3);

                Plane plane = new Plane(p1, p2, p3);

                planes.add(plane);

                triangle.setPlane(plane);

//                System.out.println("Ravnina trokuta " + triangle + ": " + plane);
            }


            minMaxSet = false;

            for(Point3D vertex: vertices){
                testMinMax(vertex.x, vertex.y, vertex.z);
            }

            dx = xMax - xMin;
            dy = yMax - yMin;
            dz = zMax - zMin;
            sTick = new Point3D((xMin + xMax) / 2, (yMin + yMax) / 2, (zMin + zMax) / 2);

//            if(dx == 2 && dy == 2 && dz == 2){
//                scalePoints(2);
//                scaled = true;
//            }

            calculateNormals();

        } catch (IOException | ReadOnlyException e) {
            e.printStackTrace();
        }
    }

    private void testMinMax(double x, double y, double z){
        if(!minMaxSet){
            this.xMin = x;
            this.xMax = x;
            this.yMin = y;
            this.yMax = y;
            this.zMin = z;
            this.zMax = z;
            minMaxSet = true;
        }
        if(x < xMin)
            xMin = x;
        else if(x > xMax)
            xMax = x;
        if(y < yMin)
            yMin = y;
        else if(y > yMax)
            yMax = y;
        if(z < zMin)
            zMin = z;
        else if(z > zMax)
            zMax = z;
    }

    private void translatePoints(){
        for(Point3D vertex: vertices){
            vertex.translate(sTick);
        }
    }

    private void scalePoints(double divisor){
        for(Point3D vertex: vertices){
            vertex.x /= divisor;
            vertex.y /= divisor;
            vertex.z /= divisor;
        }
    }

    public int getNoVertices() {
        return noVertices;
    }

    public int getNoPolygons() {
        return noPolygons;
    }

    public Point3D getVertex(int i){
        return vertices.get(i);
    }

    public boolean isScaled() {
        return scaled;
    }

    public final List<Point3D> getPolygon(int i){
        List<Point3D> retList = new ArrayList<>();
        Triangle retTriangle = triangles.get(i);
        Point3D p1 = vertices.get(retTriangle.p1);
        Point3D p2 = vertices.get(retTriangle.p2);
        Point3D p3 = vertices.get(retTriangle.p3);
        retList.add(p1);
        retList.add(p2);
        retList.add(p3);
        return retList;
    }

    public boolean testPoint(Point3D point){
        boolean flag = true;
        for(Plane plane: planes){
            if (!testPointAgainstPlane(point, plane)){
                flag = false;
                break;
            }
        }
        return flag;
    }

    private boolean testPointAgainstPlane(Point3D point, Plane plane){
        double value = point.x * plane.getA() + point.y * plane.getB() + point.z * plane.getC() + plane.getD();
        return value <= 0;
    }

    public boolean triangleVisible(int index){
        return triangles.get(index).isVisible();
    }

    public void determineFaceVisibilities1(IVector eye){
        for(Triangle triangle: triangles){
            double mul = triangle.getA() * eye.get(0) + triangle.getB() * eye.get(1) + triangle.getC() * eye.get(2) + triangle.getD();
            triangle.setVisible(mul > 0);
        }

    }

    public void determineFaceVisibilities2(IVector eye){
        for(Triangle triangle: triangles){
            Point3D p1 = vertices.get(triangle.p1);
            Point3D p2 = vertices.get(triangle.p2);
            Point3D p3 = vertices.get(triangle.p3);

            double[] cElements = new double[3];

            cElements[0] = (p1.x + p2.x + p3.x) / 3;
            cElements[1] = (p1.y + p2.y + p3.y) / 3;
            cElements[2] = (p1.z + p2.z + p3.z) / 3;

            IVector c = new Vector(cElements);

            double[] nElements = new double[3];

            nElements[0] = triangle.getA();
            nElements[1] = triangle.getB();
            nElements[2] = triangle.getC();

            IVector n = new Vector(nElements);
            IVector e;

            try {
                e = eye.nSub(c);
                triangle.setVisible(n.scalarProduct(e) > 0);
            } catch (IncompatibleOperandException | ReadOnlyException incompatibleOperandException) {
                incompatibleOperandException.printStackTrace();
            }


        }
    }

    private void calculateNormals() throws ReadOnlyException {
        double[] x = new double[this.noVertices];
        double[] y = new double[this.noVertices];
        double[] z = new double[this.noVertices];
        int[] c = new int[this.noVertices];


        for(int i = 0; i < noPolygons; i++){
            Triangle triangle = triangles.get(i);
            IVector triangleNormal = new Vector(triangle.getA(), triangle.getB(), triangle.getC()).nFromHomogeneous().normalize();

            int[] indexes = new int[]{triangle.getP1(), triangle.getP2(), triangle.getP3()};

            for(int k = 0; k < 3; k++){
                x[indexes[k]] += triangleNormal.get(0);
                y[indexes[k]] += triangleNormal.get(1);
                z[indexes[k]] += triangleNormal.get(2);
                c[indexes[k]]++;
            }

        }

        for(int i = 0; i < noVertices; i++){
            IVector normal = new Vector(new double[]{x[i], y[i], z[i], c[i]}).nFromHomogeneous().normalize();
            vertices.get(i).setNormal(normal);
        }
    }

    public IVector getPolygonNormal(int index) throws ReadOnlyException {
        Triangle triangle = triangles.get(index);
        return (new Vector(triangle.getA(), triangle.getB(), triangle.getC())).nFromHomogeneous().normalize();
    }
}
