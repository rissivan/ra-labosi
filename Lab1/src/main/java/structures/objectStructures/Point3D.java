package structures.objectStructures;

import structures.linalg.vector.IVector;

public class Point3D {

    double x, y, z;

    private IVector normal;

    public Point3D(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point3D(IVector vector){
        this.x = vector.get(0);
        this.y = vector.get(1);
        this.z = vector.get(2);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public void translate(Point3D center){
        this.x -= center.x;
        this.y -= center.y;
        this.z -= center.z;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ", " + z + ")";
    }

    public IVector getNormal() {
        return normal;
    }

    public void setNormal(IVector normal) {
        this.normal = normal;
    }
}
