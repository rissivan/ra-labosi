package structures.util;

import com.jogamp.opengl.awt.GLCanvas;
import structures.linalg.vector.IVector;
import structures.linalg.vector.Vector;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class EyePointData {

    private final double eyeX;
    private final double eyeY;
    private final double eyeZ;

    private double angle;
    private double increment;
    private double r;

    private GLCanvas glCanvas;

    public EyePointData(double x, double y, double z){
        this.eyeX = x;
        this.eyeY = y;
        this.eyeZ = z;

        angle = Math.atan(eyeY / eyeX) * 180 / Math.PI;
        increment = 1;
        r = Math.sqrt(eyeX * eyeX + eyeY * eyeY);
    }

    public void setGlCanvas(GLCanvas glCanvas) {
        this.glCanvas = glCanvas;
    }

    public void setIncrement(double increment) {
        this.increment = increment;
    }

    public double getAngle() {
        return angle;
    }

    public double getIncrement() {
        return increment;
    }

    public double getR() {
        return r;
    }

    public KeyAdapter getKeyAdapter() {
        return keyAdapter;
    }

    private final KeyAdapter keyAdapter = new KeyAdapter() {
        @Override
        public void keyPressed(KeyEvent e) {
            e.consume();
            if(e.getKeyCode() == KeyEvent.VK_L) {
                angle -= increment;
                if(angle < 0)
                    angle += 360;
            } else if (e.getKeyCode() == KeyEvent.VK_R) {
                angle += increment;
                if(angle > 360)
                    angle -= 360;
            } else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                angle = Math.atan(eyeY / eyeX) * 180 / Math.PI;;
            }
            glCanvas.display();
        }

    };

    public void increment(){
        angle += increment;
        if(angle > 360)
            angle -= 360;
    }

    public void decrement(){
        angle -= increment;
        if(angle < 0)
            angle += 360;
    }

    public void reset(){
        angle = Math.atan(eyeY / eyeX) * 180 / Math.PI;;
    }

    public IVector calculateEye(){
        double angleRad = angle / 180 * Math.PI;
        double xx = r * Math.cos(angleRad);
        double yy = r * Math.sin(angleRad);
        return new Vector(xx, yy, eyeZ);
    }

    public IVector getEye(){
        IVector v = calculateEye();
        double[] elements = new double[3];
        elements[0] = v.get(0);
        elements[1] = v.get(1);
        elements[2] = v.get(2);
        return new Vector(elements);
    }
}
