package structures.linalg.util;

public class IncompatibleOperandException extends Exception{

    public IncompatibleOperandException(String message){
        super(message);
    }
}
