package structures.linalg.util;

public class ReadOnlyException extends Exception {

    public ReadOnlyException(String message){
        super (message);
    }
}
