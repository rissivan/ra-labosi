package structures.linalg.vector;

import structures.linalg.matrix.IMatrix;
import structures.linalg.util.ReadOnlyException;

import java.text.DecimalFormat;

public class VectorMatrixView extends AbstractVector {

    private IMatrix original;
    private int dimension;
    private boolean rowMatrix;

    public VectorMatrixView(IMatrix original){
        this.original = original;
        if (this.original.getRowsCount() == 1){
            rowMatrix = true;
            dimension = this.original.getColsCount();
        } else {
            rowMatrix = false;
            dimension = this.original.getRowsCount();
        }
    }

    @Override
    public double get(int index) {
        if (rowMatrix)
            return this.original.get(0, index);
        else
            return this.original.get(index, 0);
    }

    @Override
    public IVector set(int index, double value) throws ReadOnlyException {
        if (rowMatrix)
            return new VectorMatrixView(this.original.set(0, index, value));
        else
            return new VectorMatrixView(this.original.set(index, 0, value));
    }

    @Override
    public int getDimension() {
        return dimension;
    }

    @Override
    public IVector copy() {
        double[] elements = new double[dimension];

        for (int i = 0; i < dimension; i++){
            if (rowMatrix)
                elements[i] = this.original.get(0, i);
            else
                elements[i] = this.original.get(i, 0);
        }

        return new Vector(false, true, elements);
    }

    @Override
    public Vector newInstance(int x) {
        return new Vector(new double[x]);
    }

    @Override
    public String toString(int precision) {         //TODO: SREDITI!
        int dim = this.dimension;
        StringBuilder format = new StringBuilder();
        format.append("#.");
        for (int i = 0; i < precision; i++)
            format.append("#");
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        DecimalFormat df = new DecimalFormat(format.toString());
        for (int i = 0; i < dim; i++){
            sb.append(df.format(this.rowMatrix ? this.original.get(0, i) : this.original.get(i, 0)));
            if (i < dim - 1){
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    @Override
    public String toString() {
        return toString(2);
    }
}
