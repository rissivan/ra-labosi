package structures.linalg.vector;

import structures.linalg.matrix.IMatrix;
import structures.linalg.matrix.MatrixVectorView;
import structures.linalg.util.IncompatibleOperandException;
import structures.linalg.util.ReadOnlyException;

public abstract class AbstractVector implements IVector {

    public AbstractVector(){}

    @Override
    public abstract double get(int index);

    @Override
    public abstract IVector set(int index, double value) throws ReadOnlyException;

    @Override
    public abstract int getDimension();

    @Override
    public abstract IVector copy();

    @Override
    public IVector copyPart(int x) throws ReadOnlyException {
        IVector copiedVector = newInstance(x);
        int min = Math.min(x, this.getDimension());
        for (int i = 0; i < min; i++){
            copiedVector.set(i, this.get(i));
        }
        return copiedVector;
    }

    @Override
    public abstract IVector newInstance(int x);

    @Override
    public IVector add(IVector other) throws IncompatibleOperandException, ReadOnlyException {
        if (this.getDimension() != other.getDimension()){
            throw new IncompatibleOperandException("Vector dimensions do not match.");
        }
        int dim = this.getDimension();
        for (int i = 0; i < dim; i++){
            this.set(i, this.get(i) + other.get(i));
        }
        return this;
    }

    @Override
    public IVector nAdd(IVector other) throws IncompatibleOperandException, ReadOnlyException {
        return this.copy().add(other);
    }

    @Override
    public IVector sub(IVector other) throws IncompatibleOperandException, ReadOnlyException {
        if (this.getDimension() != other.getDimension()){
            throw new IncompatibleOperandException("Vector dimensions do not match.");
        }
        int dim = this.getDimension();
        for (int i = 0; i < dim; i++){
            this.set(i, this.get(i) - other.get(i));
        }
        return this;
    }

    @Override
    public IVector nSub(IVector other) throws IncompatibleOperandException, ReadOnlyException {
        return this.copy().sub(other);
    }

    @Override
    public IVector scalarMultiply(double scalar) throws ReadOnlyException {
        int dim = this.getDimension();
        for (int i = 0; i < dim; i++){
            this.set(i, this.get(i) * scalar);
        }
        return this;
    }

    @Override
    public IVector nScalarMultiply(double scalar) throws ReadOnlyException {
        return this.copy().scalarMultiply(scalar);
    }

    @Override
    public double norm() {
        double sum = 0;
        int dim = this.getDimension();
        for (int i = 0; i < dim; i++){
            sum += this.get(i) * this.get(i);
        }
        return Math.sqrt(sum);
    }

    @Override
    public IVector normalize() throws ReadOnlyException {
        int dim = this.getDimension();
        double norm = this.norm();
        for (int i = 0; i < dim; i++){
            this.set(i, this.get(i) / norm);
        }
        return this;
    }

    @Override
    public double cosine(IVector other) {
        return (this.scalarProduct(other)) / (this.norm() * other.norm());
    }

    @Override
    public double scalarProduct(IVector other) {
        int dim = this.getDimension();
        double product = 0;
        for (int i = 0; i < dim; i++){
            product += this.get(i) * other.get(i);
        }
        return product;
    }

    @Override
    public IVector nVectorProduct(IVector other) throws IncompatibleOperandException, ReadOnlyException {
        if (this.getDimension() != other.getDimension()){
            throw new IncompatibleOperandException("Can't calculate a cross product of non-3D vectors");
        }
        IVector crossProduct = newInstance(this.getDimension());
        crossProduct.set(0, this.get(1) * other.get(2) -
                            this.get(2) * other.get(1));
        crossProduct.set(1, - this.get(0) * other.get(2) +
                              this.get(2) * other.get(0));
        crossProduct.set(2, this.get(0) * other.get(1) -
                            this.get(1) * other.get(0));
        if(this.getDimension() == 4)
            crossProduct.set(3, 1);
        return crossProduct;
    }

    @Override
    public IVector nFromHomogeneous() throws ReadOnlyException {
        int dim = this.getDimension();
        double homogeneous = this.get(dim - 1);
        IVector newVector = newInstance(dim - 1);
        for (int i = 0; i < dim - 1; i++){
            newVector.set(i, this.get(i) / homogeneous);
        }
        return newVector;
    }

    @Override
    public IMatrix toRowMatrix(boolean flag) {      //TODO: Srediti flagove
        return new MatrixVectorView(this, true);
    }

    @Override
    public IMatrix toColumnMatrix(boolean flag) {
        return new MatrixVectorView(this, false);
    }

    @Override
    public double[] toArray() {
        int dim = this.getDimension();
        double[] ret = new double[dim];
        for (int i = 0; i < dim; i++){
            ret[i] = this.get(i);
        }
        return ret;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
