package structures.linalg.vector;

import structures.linalg.matrix.IMatrix;
import structures.linalg.util.IncompatibleOperandException;
import structures.linalg.util.ReadOnlyException;

public interface IVector {

    double get(int index);
    IVector set(int index, double value) throws ReadOnlyException;
    int getDimension();
    IVector copy();
    IVector copyPart(int x) throws ReadOnlyException;
    IVector newInstance(int x);
    IVector add(IVector other) throws IncompatibleOperandException, ReadOnlyException;
    IVector nAdd(IVector other) throws IncompatibleOperandException, ReadOnlyException;
    IVector sub(IVector other) throws IncompatibleOperandException, ReadOnlyException;
    IVector nSub(IVector other) throws IncompatibleOperandException, ReadOnlyException;
    IVector scalarMultiply(double scalar) throws ReadOnlyException;
    IVector nScalarMultiply(double scalar) throws ReadOnlyException;
    double norm();
    IVector normalize() throws ReadOnlyException;
    double cosine(IVector other);
    double scalarProduct(IVector other);
    IVector nVectorProduct(IVector other) throws IncompatibleOperandException, ReadOnlyException;
    IVector nFromHomogeneous() throws ReadOnlyException;
    IMatrix toRowMatrix(boolean flag);
    IMatrix toColumnMatrix(boolean flag);
    double[] toArray();

    String toString(int i);
}
