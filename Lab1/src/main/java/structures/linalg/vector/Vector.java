package structures.linalg.vector;

import structures.linalg.util.ReadOnlyException;

import java.text.DecimalFormat;
import java.util.Arrays;

public class Vector extends AbstractVector {

    private double[] elements;
    private int dimension;
    private boolean readOnly;

    public Vector(double[] elements){
        this.elements = elements;
        this.dimension = elements.length;
        this.readOnly = false;
    }

    public Vector(double x, double y, double z){
        elements = new double[4];
        elements[0] = x;
        elements[1] = y;
        elements[2] = z;
        elements[3] = 1;
        dimension = 4;
        readOnly = false;
    }

    public Vector(boolean readOnly, boolean sameArray, double[] elements){
        if (sameArray){
            this.elements = elements;
        } else {
            this.elements = elements.clone();
        }
        this.dimension = elements.length;
        this.readOnly = readOnly;
    }



    @Override
    public double get(int index) {
        return this.elements[index];
    }

    @Override
    public IVector set(int index, double value) throws ReadOnlyException {
        if (this.readOnly){
            throw new ReadOnlyException("Vector is read-only and its elements can therefore not be edited");
        }
        this.elements[index] = value;
        return this;
    }

    @Override
    public int getDimension() {
        return this.dimension;
    }

    @Override
    public IVector copy() {
        return new Vector(readOnly, false, elements);
    }

    @Override
    public IVector newInstance(int x) {
        return new Vector(new double[x]);
    }

    public static Vector parseSimple(String input){
        double[] elements = Arrays.stream(input.trim().split(" +")).mapToDouble(Double:: parseDouble).toArray();
        return new Vector(elements);
    }

    @Override
    public String toString(int precision) {
        int dim = this.dimension;
        StringBuilder format = new StringBuilder();
        format.append("#.");
        for (int i = 0; i < precision; i++)
            format.append("#");
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        DecimalFormat df = new DecimalFormat(format.toString());
        for (int i = 0; i < dim; i++){
            sb.append(df.format(this.elements[i]));
            if (i < dim - 1){
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    @Override
    public String toString() {
        return toString(2);
    }
}
