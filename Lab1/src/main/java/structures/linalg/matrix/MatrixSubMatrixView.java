package structures.linalg.matrix;

import structures.linalg.util.ReadOnlyException;

public class MatrixSubMatrixView extends AbstractMatrix {

    private IMatrix originalMatrix;
    private int[] rowIndexes;
    private int[] colIndexes;

    public MatrixSubMatrixView(IMatrix originalMatrix, int row, int col){
        this.originalMatrix = originalMatrix;
        int dimR = this.originalMatrix.getRowsCount();
        int dimC = this.originalMatrix.getColsCount();

        this.rowIndexes = new int[dimR - 1];
        this.colIndexes = new int[dimC - 1];

        for (int i = 0, k = 0; i < dimR; i++){
            if (i != row)
                this.rowIndexes[k++] = i;
        }
        for (int j = 0, k = 0; j < dimC; j++){
            if (j != col)
                this.colIndexes[k++] = j;
        }

    }

    private MatrixSubMatrixView(IMatrix originalMatrix, int[] rowIndexes, int[] colIndexes){
        this.originalMatrix = originalMatrix;
        this.rowIndexes = rowIndexes;
        this.colIndexes = colIndexes;
    }

    @Override
    public int getRowsCount() {
        return this.rowIndexes.length;
    }

    @Override
    public int getColsCount() {
        return this.colIndexes.length;
    }

    @Override
    public double get(int x, int y) {
        return this.originalMatrix.get(rowIndexes[x], colIndexes[y]);
    }

    @Override
    public IMatrix set(int x, int y, double value) throws ReadOnlyException {
        return this.originalMatrix.set(rowIndexes[x], colIndexes[y], value);            //provjeriti, dodati najvjerojatnije novu instancu prek privatnog konstruktora
    }

    @Override
    public IMatrix copy() {
        int rows = rowIndexes.length;
        int cols = colIndexes.length;
        double[][] retVal = new double[rows][cols];

        for (int i = 0; i < rows; i++){
            for (int j = 0; j < cols; j++){
                retVal[i][j] = originalMatrix.get(rowIndexes[i], colIndexes[j]);
            }
        }

        return new Matrix(rows, cols, retVal, true);
    }

    @Override
    public IMatrix newInstance(int noRows, int noCols) {
        return new Matrix(noRows, noCols);
    }

    @Override
    public IMatrix subMatrix(int row, int col, boolean liveView) {
        int dimR = rowIndexes.length;
        int dimC = colIndexes.length;

        int[] newRowIndexes = new int[dimR - 1];
        int[] newColIndexes = new int[dimC - 1];

        for (int i = 0, k = 0; i < dimR; i++){
            if (i != row)
                newRowIndexes[k++] = rowIndexes[i];
        }

        for (int j = 0, k = 0; j < dimC; j++){
            if (j != col){
                newColIndexes[k++] = colIndexes[j];
            }
        }

        IMatrix retval = new MatrixSubMatrixView(originalMatrix, newRowIndexes, newColIndexes);

        if (liveView)
            return retval;
        else
            return retval.copy();
    }
}
