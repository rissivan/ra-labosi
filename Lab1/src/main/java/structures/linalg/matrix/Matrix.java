package structures.linalg.matrix;

import java.text.DecimalFormat;
import java.util.Arrays;

public class Matrix extends AbstractMatrix {

    protected double elements[][];
    protected int rows;
    protected int cols;

    public Matrix(int rows, int cols){
        this.rows = rows;
        this.cols = cols;
        this.elements = new double[rows][cols];
    }

    public Matrix(int rows, int cols, double[][] elements, boolean live){
        this.rows = rows;
        this.cols = cols;
        if (live){
            this.elements = elements;
        } else {
            this.elements = elements.clone();
        }
    }

    @Override
    public int getRowsCount() {
        return this.rows;
    }

    @Override
    public int getColsCount() {
        return this.cols;
    }

    @Override
    public double get(int x, int y) {
        return elements[x][y];
    }

    @Override
    public IMatrix set(int x, int y, double value) {
        this.elements[x][y] = value;
        return this;
    }

    @Override
    public IMatrix copy() {
        return new Matrix(rows, cols, elements, false);
    }

    @Override
    public IMatrix newInstance(int noRows, int noCols) {
        return new Matrix(noRows, noCols);
    }

    public String toString(int precision){
        StringBuilder format = new StringBuilder();
        format.append("#.");
        for (int i = 0; i < precision; i++)
            format.append("#");
        DecimalFormat df = new DecimalFormat(format.toString());
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < rows; i++){
            sb.append("[");
            for (int j = 0; j < cols; j++){
                sb.append(df.format(this.elements[i][j]));
                if (j < cols - 1){
                    sb.append(", ");
                }
            }
            sb.append("]");
            if (i < rows - 1){
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        return toString(3);
    }

    public static Matrix parseSimple(String input){
        String[] rowArray = input.trim().split("\\|");
        double[] firstRow = Arrays.stream(rowArray[0].trim().split(" +")).mapToDouble(Double:: parseDouble).toArray();

        int r = rowArray.length;
        int c = firstRow.length;

        double[][] elementsMatrix = new double[rowArray.length][firstRow.length];
        for (int i = 0; i < r; i++){
            elementsMatrix[i] = Arrays.stream(rowArray[i].trim().split(" +")).mapToDouble(Double:: parseDouble).toArray();
        }

        return new Matrix(r, c, elementsMatrix, true);
    }
}
