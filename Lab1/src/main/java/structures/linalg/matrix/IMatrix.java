package structures.linalg.matrix;

import structures.linalg.util.IncompatibleOperandException;
import structures.linalg.util.ReadOnlyException;
import structures.linalg.vector.IVector;

public interface IMatrix {

    int getRowsCount();
    int getColsCount();
    double get(int x, int y);
    IMatrix set(int x, int y, double value) throws ReadOnlyException;
    IMatrix copy();
    IMatrix newInstance(int noRows, int noCols);
    IMatrix nTranspose(boolean liveView);
    IMatrix add(IMatrix other) throws IncompatibleOperandException, ReadOnlyException;
    IMatrix nAdd(IMatrix other) throws IncompatibleOperandException, ReadOnlyException;
    IMatrix sub(IMatrix other) throws IncompatibleOperandException, ReadOnlyException;
    IMatrix nSub(IMatrix other) throws IncompatibleOperandException, ReadOnlyException;
    IMatrix nMultiply(IMatrix other) throws IncompatibleOperandException, ReadOnlyException;
    double determinant() throws IncompatibleOperandException;
    IMatrix subMatrix(int row, int col, boolean liveView);
    IMatrix nInvert() throws IncompatibleOperandException, ReadOnlyException;
    double[][] toArray();
    IVector toVector(boolean fromRowMatrix);
}
