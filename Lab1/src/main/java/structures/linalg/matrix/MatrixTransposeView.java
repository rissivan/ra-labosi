package structures.linalg.matrix;

import structures.linalg.util.ReadOnlyException;

import java.text.DecimalFormat;

public class MatrixTransposeView extends AbstractMatrix {

    private IMatrix originalMatrix;

    public MatrixTransposeView(IMatrix matrix){
        this.originalMatrix = matrix;
    }

    @Override
    public int getRowsCount() {
        return this.originalMatrix.getColsCount();
    }

    @Override
    public int getColsCount() {
        return this.originalMatrix.getRowsCount();
    }

    @Override
    public double get(int x, int y) {
        return this.originalMatrix.get(y, x);
    }

    @Override
    public IMatrix set(int x, int y, double value) throws ReadOnlyException {
        return this.originalMatrix.set(y, x, value);        //provjeriti
    }

    @Override
    public IMatrix copy() {
        return new Matrix(this.getRowsCount(), this.getColsCount(), this.toArray().clone(), true);
    }

    @Override
    public IMatrix newInstance(int noRows, int noCols) {
        return new Matrix(noRows, noCols);
    }

    @Override
    public double[][] toArray(){
        int dimR = this.originalMatrix.getRowsCount();
        int dimC = this.originalMatrix.getColsCount();

        double[][] retVal = new double[dimC][dimR];

        for (int i = 0; i < dimR; i++){
            for (int j = 0; j < dimC; j++){
                retVal[j][i] = this.originalMatrix.get(i, j);
            }
        }
        return retVal;
    }

    @Override
    public String toString() {
        return toString(2);
    }

    public String toString(int precision){
        StringBuilder format = new StringBuilder();
        format.append("#.");
        for (int i = 0; i < precision; i++)
            format.append("#");
        DecimalFormat df = new DecimalFormat(format.toString());
        StringBuilder sb = new StringBuilder();

        int rows = this.originalMatrix.getColsCount();
        int cols = this.originalMatrix.getRowsCount();

        for (int j = 0; j < cols; j++){
            sb.append("[");
            for (int i = 0; i < rows; i++){
                sb.append(df.format(this.originalMatrix.get(i, j)));
                if (i < rows - 1){
                    sb.append(", ");
                }
            }
            sb.append("]");
            if (j < rows - 1){
                sb.append("\n");
            }
        }
        return sb.toString();
    }
}
