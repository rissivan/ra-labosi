package structures.linalg.matrix;

import structures.linalg.util.ReadOnlyException;
import structures.linalg.vector.IVector;

public class MatrixVectorView extends AbstractMatrix {

    private boolean asRowMatrix;
    private IVector originalVector;

    public MatrixVectorView(IVector originalVector, boolean asRowMatrix){
        this.originalVector = originalVector;
        this.asRowMatrix = asRowMatrix;
    }

    @Override
    public int getRowsCount() {
        return (asRowMatrix ? 1 : this.originalVector.getDimension());
    }

    @Override
    public int getColsCount() {
        return (asRowMatrix ? this.originalVector.getDimension() : 1);
    }

    @Override
    public double get(int x, int y) {
        if (asRowMatrix)
            return this.originalVector.get(y);
        else
            return this.originalVector.get(x);
    }

    @Override
    public IMatrix set(int x, int y, double value) throws ReadOnlyException {
        if (asRowMatrix)
            return new MatrixVectorView(this.originalVector.set(y, value), true);
        else
            return new MatrixVectorView(this.originalVector.set(x, value), false);
    }

    @Override
    public IMatrix copy() {
        int dimR, dimC, dimV;
        if (this.asRowMatrix){
            dimR = 1;
            dimC = this.originalVector.getDimension();
            dimV = dimC;
        } else {
            dimR = this.originalVector.getDimension();
            dimC = 1;
            dimV = dimR;
        }

        double[][] newMatrix = new double[dimR][dimC];

        for (int i = 0; i < dimV; i++){
            if (this.asRowMatrix)
                newMatrix[0][i] = this.originalVector.get(i);
            else
                newMatrix[i][0] = this.originalVector.get(i);
        }

        return new Matrix(dimR, dimC, newMatrix, true);
    }

    @Override
    public IMatrix newInstance(int noRows, int noCols) {
        return new Matrix(noRows, noCols);
    }

    @Override
    public String toString() {
        return originalVector.toString();
    }
}
