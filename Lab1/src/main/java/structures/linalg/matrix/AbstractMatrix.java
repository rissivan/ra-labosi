package structures.linalg.matrix;

import structures.linalg.util.IncompatibleOperandException;
import structures.linalg.util.ReadOnlyException;
import structures.linalg.vector.IVector;
import structures.linalg.vector.VectorMatrixView;

public abstract class AbstractMatrix implements IMatrix {

    public AbstractMatrix(){}

    @Override
    public abstract int getRowsCount();

    @Override
    public abstract int getColsCount();

    @Override
    public abstract double get(int x, int y);

    @Override
    public abstract IMatrix set(int x, int y, double value) throws ReadOnlyException;

    @Override
    public abstract IMatrix copy();

    @Override
    public abstract IMatrix newInstance(int noRows, int noCols);

    @Override
    public IMatrix nTranspose(boolean liveView) {       //nije mi jasno cemu sluzi boolean
        MatrixTransposeView retval = new MatrixTransposeView(this);
        if (liveView)
            return retval;
        else
            return retval.copy();
    }

    @Override
    public IMatrix add(IMatrix other) throws IncompatibleOperandException, ReadOnlyException {
        if (this.getRowsCount() != other.getRowsCount() || this.getColsCount() != other.getColsCount()){
            throw new IncompatibleOperandException("Matrix dimensions do not match");
        }
        int dimR = this.getRowsCount();
        int dimC = this.getColsCount();
        for (int i = 0; i < dimR; i++){
            for (int j = 0; j < dimC; j++){
                this.set(i, j, this.get(i, j) + other.get(i, j));
            }
        }
        return this;
    }

    @Override
    public IMatrix nAdd(IMatrix other) throws IncompatibleOperandException, ReadOnlyException {
        return this.copy().add(other);
    }

    @Override
    public IMatrix sub(IMatrix other) throws IncompatibleOperandException, ReadOnlyException {
        if (this.getRowsCount() != other.getRowsCount() || this.getColsCount() != other.getColsCount()){
            throw new IncompatibleOperandException("Matrix dimensions do not match");
        }
        int dimR = this.getRowsCount();
        int dimC = this.getColsCount();
        for (int i = 0; i < dimR; i++){
            for (int j = 0; j < dimC; j++){
                this.set(i, j, this.get(i, j) - other.get(i, j));
            }
        }
        return this;
    }

    @Override
    public IMatrix nSub(IMatrix other) throws IncompatibleOperandException, ReadOnlyException {
        return this.copy().sub(other);
    }

    @Override
    public IMatrix nMultiply(IMatrix other) throws IncompatibleOperandException, ReadOnlyException {

        if (this.getColsCount() != other.getRowsCount()){
            throw new IncompatibleOperandException("Matrices aren't compatible for multiplication");
        }

        int dimMutual = this.getColsCount();
        int dimR = this.getRowsCount();
        int dimC = other.getColsCount();

        IMatrix retVal = newInstance(dimR, dimC);
        for (int i = 0; i < dimR; i++){
            for (int j = 0; j < dimC; j++){
                double val = 0;
                for (int k = 0; k < dimMutual; k++){
                    val += this.get(i, k) * other.get(k, j);
                }
                retVal.set(i, j, val);
            }
        }
        return retVal;
    }

    @Override
    public double determinant() throws IncompatibleOperandException {
        if (this.getColsCount() != this.getRowsCount()){
            throw new IncompatibleOperandException("Matrix dimensions do not match");
        }
        return determinant(this);
    }

    private static double determinant(IMatrix matrix) throws IncompatibleOperandException {
        if (matrix.getRowsCount() != matrix.getColsCount()){
            throw new IncompatibleOperandException("Matrix dimensions do not match");
        }
        if (matrix.getRowsCount() == 2){
            return  matrix.get(0, 0) * matrix.get(1, 1) -
                    matrix.get(1, 0) * matrix.get(0, 1);
        }
        if (matrix.getRowsCount() == 1){
            return matrix.get(0, 0);
        }

        int dimR = matrix.getRowsCount();
        double retVal = 0;
        for (int i = 0; i < dimR; i++){
            retVal += (i % 2 == 0 ? 1 : -1) * matrix.get(i, 0) * determinant(matrix.subMatrix(i, 0, true));
        }
        return retVal;
    }


    @Override
    public IMatrix subMatrix(int row, int col, boolean liveView) {
        MatrixSubMatrixView retVal = new MatrixSubMatrixView(this, row, col);

        if (liveView)
            return retVal;
        else
            return retVal.copy();
    }

    @Override
    public IMatrix nInvert() throws IncompatibleOperandException, ReadOnlyException {
        int dimR = this.getRowsCount();
        int dimC = this.getColsCount();
        IMatrix cofactorMatrix = newInstance(this.getRowsCount(), this.getColsCount());
        for (int i = 0; i < dimR; i++){
            for (int j = 0; j < dimC; j++){
                cofactorMatrix.set(i, j, (((i + j) % 2 == 0) ? 1 : -1) * this.subMatrix(i, j, true).determinant());
            }
        }
        double det = this.determinant();

//        System.out.println("COF:" + cofactorMatrix);
//        System.out.println();

        IMatrix adjugateMatrix = cofactorMatrix.nTranspose(true);

//        System.out.println("ADJ: " + adjugateMatrix);
//        System.out.println();

        for (int i = 0; i < dimC; i++){
            for (int j = 0; j < dimR; j++){         //matrica je transponirana pa je potrebno zamijeniti varijable
                adjugateMatrix.set(i, j, adjugateMatrix.get(i, j) / det);
            }
        }
        return adjugateMatrix;
    }

    @Override
    public double[][] toArray() {
        int dimR = this.getRowsCount();
        int dimC = this.getColsCount();

        double[][] retVal = new double[dimR][dimC];

        for (int i = 0; i < dimR; i++){
            for (int j = 0; j < dimC; j++){
                retVal[i][j] = this.get(i, j);
            }
        }
        return retVal;
    }

    @Override
    public IVector toVector(boolean fromRowMatrix) {    //TODO: Srediti flag
        return new VectorMatrixView(this);
    }
}
