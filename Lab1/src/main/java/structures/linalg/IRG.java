package structures.linalg;

import structures.linalg.matrix.IMatrix;
import structures.linalg.matrix.Matrix;
import structures.linalg.util.IncompatibleOperandException;
import structures.linalg.util.ReadOnlyException;
import structures.linalg.vector.IVector;
import structures.linalg.vector.Vector;
import structures.objectStructures.Point3D;

public class IRG {
    public static IMatrix translate3D(float dx, float dy, float dz){
        double[][] elements = new double[4][4];
        for(int i = 0; i < 4; i++)
            elements[i][i] = 1;
        elements[3][0] = dx;
        elements[3][1] = dy;
        elements[3][2] = dz;
        return new Matrix(4, 4, elements, false);
    }

    public static IMatrix scale3D(float sx, float sy, float sz){
        double[][] elements = new double[4][4];
        elements[0][0] = sx;
        elements[1][1] = sy;
        elements[2][2] = sz;
        elements[3][3] = 1;
        return new Matrix(4, 4, elements, false);
    }

    public static IMatrix lookAtMatrix(IVector eye, IVector center, IVector viewUp) throws ReadOnlyException, IncompatibleOperandException {
        double[][] elements = new double[4][4];

        Matrix t1 = new Matrix(4, 4, new double[][]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {-center.get(0), -center.get(1), center.get(2), 1}
        }, false);


        double[] eyeElements = new double[3];
        double[] centerElements = new double[3];
        double[] viewUpElements = new double[3];

        for(int i = 0; i < 3; i++){
            eyeElements[i] = eye.get(i);
            centerElements[i] = center.get(i);
            viewUpElements[i] = viewUp.get(i);
        }

        IVector eye3 = new Vector(eyeElements);
        IVector center3 = new Vector(centerElements);
        IVector viewUp3 = new Vector(viewUpElements);

        IVector N = eye3.nSub(center3).normalize();
        viewUp3 = viewUp3.normalize();
        IVector U = viewUp3.nVectorProduct(N).normalize();
        IVector V = N.nVectorProduct(U);

        for(int i = 0; i < 3; i++){
            elements[i][0] = U.get(i);
            elements[i][1] = V.get(i);
            elements[i][2] = N.get(i);
        }
        elements[3][3] = 1;
        elements[3][2] = -eye3.nSub(center3).norm();

        Matrix t2 = new Matrix(4, 4, elements, false);

        return t1.nMultiply(t2);
    }

    public static IMatrix buildFrustumMatrix(double l, double r, double b, double t, int n, int f){
        double[][] elements = new double[4][4];
        elements[0][0] = ((double) 2 * n) / (r - l);
        elements[1][1] = ((double) 2 * n) / (t - b);
        elements[2][0] = (r + l) / (r - l);
        elements[2][1] = (t + b) / (t - b);
        elements[2][2] = - ((double) (f + n)) / (f - n);
        elements[2][3] = -1;
        elements[3][2] = ((double) (-2 * f * n)) / (f - n);
        return new Matrix(4, 4, elements, false);
    }

    public static boolean isAntiClockwise(Point3D p1, Point3D p2, Point3D p3){
        double[] x = new double[4];
        double[] y = new double[4];
        double[] a = new double[3];
        double[] b = new double[3];
        double[] c = new double[3];

        x[0] = p1.getX();
        y[0] = p1.getY();
        x[1] = p2.getX();
        y[1] = p2.getY();
        x[2] = p3.getX();
        y[2] = p3.getY();
        x[3] = p1.getX();
        y[3] = p1.getY();

        for(int i = 0; i < 3; i++){
            a[i] = y[i] - y [i + 1];
            b[i] = -x[i] + x[i + 1];
            c[i] = x[i] * y[i + 1] - x[i + 1] * y[i];
        }

        boolean ccw = true;
        for(int i = 0; i < 3; i++){
            int j = i + 2 - (i <= 3 - 2 ? 0 : 3);
            if (x[j] * a[i] + y[j] * b[i] + c[i] <= 0) {
                ccw = false;
                break;
            }
        }

        return ccw;
    }
}
