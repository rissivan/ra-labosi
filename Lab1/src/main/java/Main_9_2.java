import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import structures.linalg.util.IncompatibleOperandException;
import structures.linalg.util.ReadOnlyException;
import structures.linalg.vector.IVector;
import structures.linalg.vector.Vector;
import structures.objectStructures.Object3D;
import structures.objectStructures.Point3D;
import structures.util.EyePointData;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class Main_9_2 {

    static{
        GLProfile.initSingleton();
    }

    private enum ShadingMode{
        FLAT,
        GOURAUD
    };

    static Object3D object3D;

    static final EyePointData eyePointData = new EyePointData(3, 4, 1);

    private enum Modes{
        NO_REJECTION,
        REJECT_ALG1,
        REJECT_ALG2,
    }

    static ShadingMode shadingMode = ShadingMode.FLAT;
    static Modes mode = Modes.NO_REJECTION;
    static boolean z = false;


    static final IVector lightVector = new Vector(new double[]{4, 5, 3});
    static final double[] i_a = new double[]{0.2, 0.2, 0.2};
    static final double[] i_d = new double[]{0.8, 0.8, 0};
    static final double[] i_r = new double[]{0, 0, 0};

    static final double[] k_a = new double[]{1, 1, 1};
    static final double[] k_d = new double[]{1, 1, 1};
    static final double[] k_r = new double[]{0.01, 0.01, 0.01};
    static final int shininess = 96;

    public static void main(String[] args) {

        String path = args[0];
        object3D = new Object3D(path);

        SwingUtilities.invokeLater(() -> {
            final GLProfile glProfile = GLProfile.getDefault();
            final GLCapabilities glCapabilities = new GLCapabilities(glProfile);
            final GLCanvas glCanvas = new GLCanvas(glCapabilities);

            glCanvas.addKeyListener(new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    e.consume();

                    if(e.getKeyCode() == KeyEvent.VK_1){
                        mode = Modes.NO_REJECTION;
                    } else if (e.getKeyCode() == KeyEvent.VK_2){
                        mode = Modes.REJECT_ALG1;
                    } else if (e.getKeyCode() == KeyEvent.VK_3) {
                        mode = Modes.REJECT_ALG2;
                    } else if(e.getKeyCode() == KeyEvent.VK_Z){
                        z = !z;
                        System.out.println("Z-spremnik je " + (z ? "ukljucen" : "iskljucen"));
                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_K){
                        shadingMode = ShadingMode.FLAT;
                        glCanvas.display();
                    } else if (e.getKeyCode() == KeyEvent.VK_G){
                        shadingMode = ShadingMode.GOURAUD;
                        glCanvas.display();
                    }
                }
            });

            glCanvas.addKeyListener(eyePointData.getKeyAdapter());

            glCanvas.addGLEventListener(new GLEventListener() {
                @Override
                public void init(GLAutoDrawable glAutoDrawable) {
                    eyePointData.setGlCanvas(glCanvas);
                }

                @Override
                public void dispose(GLAutoDrawable glAutoDrawable) {

                }

                @Override
                public void display(GLAutoDrawable glAutoDrawable) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();
                    gl2.glClearColor(1, 1, 1, 0);
                    if(z){
                        gl2.glEnable(GL2.GL_DEPTH_TEST);
                    } else {
                        gl2.glDisable(GL2.GL_DEPTH_TEST);
                    }
                    gl2.glClearColor(1, 1, 1, 0);
                    gl2.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
                    gl2.glLoadIdentity();

                    GLU glu = new GLU();
                    IVector eye = eyePointData.calculateEye();
                    glu.gluLookAt(eye.get(0), eye.get(1), eye.get(2), 0, 0, 0, 0, 1, 0);

                    gl2.glColor3f(0, 0, 0);

                    try {
                        renderScene(gl2);
                    } catch (ReadOnlyException | IncompatibleOperandException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();
                    gl2.glMatrixMode(GL2.GL_PROJECTION);
                    gl2.glLoadIdentity();
                    gl2.glFrustum(-0.5, 0.5, -0.5, 0.5, 1, 100);

                    gl2.glMatrixMode(GL2.GL_MODELVIEW);
                    gl2.glViewport(0, 0, width, height);
                }
            });

            final JFrame jframe = new JFrame("Zadatak 9.2");
            jframe.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            jframe.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent windowEvent) {
                    jframe.dispose();
                    System.exit(0);
                }
            });
            jframe.getContentPane().add(glCanvas, BorderLayout.CENTER);
            jframe.setSize(640,480);
            jframe.setLocationRelativeTo(null);
            jframe.setVisible(true);
            glCanvas.requestFocusInWindow();

            Point3D p1 = new Point3D(0, 0, 0);
            Point3D p2 = new Point3D(1, 0, 0);
            Point3D p3 = new Point3D(0, 1, 0);
        });
    }

    private static void renderScene(GL2 gl2) throws ReadOnlyException, IncompatibleOperandException {
        if(mode == Modes.NO_REJECTION)
            drawNoAlg(gl2);
        else if (mode == Modes.REJECT_ALG1)
            drawAlg1(gl2);
        else if (mode == Modes.REJECT_ALG2)
            drawAlg2(gl2);
    }

    private static void draw(GL2 gl2){
        int size = object3D.getNoPolygons();
        for (int i = 0; i < size; i++){
            if(mode != Modes.NO_REJECTION && !object3D.triangleVisible(i))
                continue;
            List<Point3D> polygon = object3D.getPolygon(i);
            gl2.glBegin(GL2.GL_LINE_LOOP);
            for(int j = 0; j < 3; j++){
                gl2.glVertex3d(polygon.get(j).getX(), polygon.get(j).getY(), polygon.get(j).getZ());
            }
            gl2.glEnd();
        }
    }


    private static void drawNoAlg(GL2 gl2){
        gl2.glColor3f(0.0f, 0.0f, 0.0f);
        draw(gl2);
    }

    private static void drawAlg1(GL2 gl2) throws ReadOnlyException, IncompatibleOperandException {
        object3D.determineFaceVisibilities1(eyePointData.getEye());
        drawShaded(gl2);
    }

    private static void drawAlg2(GL2 gl2) throws ReadOnlyException, IncompatibleOperandException {
        object3D.determineFaceVisibilities2(eyePointData.getEye());
        drawShaded(gl2);
    }

    private static void drawShaded(GL2 gl2) throws ReadOnlyException, IncompatibleOperandException {
        int size = object3D.getNoPolygons();
        for(int i = 0; i < size; i++){
            if(!object3D.triangleVisible(i))
                continue;
            if(shadingMode == ShadingMode.FLAT)
                drawFlat(gl2, i);
            else
                drawGouraud(gl2, i);
        }
    }

    private static void drawFlat(GL2 gl2, int index) throws ReadOnlyException, IncompatibleOperandException {
        List<Point3D> polygon = object3D.getPolygon(index);

        double x = 0, y = 0, z = 0;
        for(int i = 0; i < 3; i++){
            x += polygon.get(i).getX();
            y += polygon.get(i).getY();
            z += polygon.get(i).getZ();
        }

        x /= 3;
        y /= 3;
        z /= 3;

        double[] rgb = calculateI(new Vector(new double[]{x, y, z}), object3D.getPolygonNormal(index));
        gl2.glColor3d(rgb[0], rgb[1], rgb[2]);

        gl2.glBegin(GL2.GL_TRIANGLES);
        for(int i = 0; i < 3; i++){
            gl2.glVertex3d(polygon.get(i).getX(), polygon.get(i).getY(), polygon.get(i).getZ());
        }
        gl2.glEnd();
    }

    private static void drawGouraud(GL2 gl2, int index) throws ReadOnlyException, IncompatibleOperandException {
        List<Point3D> polygon = object3D.getPolygon(index);

        gl2.glBegin(GL2.GL_TRIANGLES);
        for(int i = 0; i < 3; i++){
            Point3D point = polygon.get(i);
            double[] rgb = calculateI(new Vector(new double[]{point.getX(), point.getY(), point.getZ()}), point.getNormal());
            gl2.glColor3d(rgb[0], rgb[1], rgb[2]);
            gl2.glVertex3d(point.getX(), point.getY(), point.getZ());
        }
        gl2.glEnd();
    }

    private static double[] calculateI(IVector vertex, IVector normal) throws ReadOnlyException, IncompatibleOperandException {
        double[] rgb = new double[]{0, 0, 0};
        double d = lightVector.nSub(vertex).norm();
        for(int i = 0; i < 3; i++){
            double cos = normal.cosine(lightVector.nSub(vertex).normalize());
            rgb[i] += i_d[i] * k_d[i] * ((cos > 0) ? cos : 0);
            rgb[i] += i_r[i] * k_r[i] * Math.pow(cosAlpha(vertex, normal), shininess);
//            rgb[i] /= (1);
            rgb[i] += i_a[i] * k_a[i];
        }
        return rgb;
    }

    private static double cosAlpha(IVector vertex, IVector normal) throws ReadOnlyException, IncompatibleOperandException {
        IVector l = lightVector.nSub(vertex).normalize();
        IVector n = normal.normalize();
        IVector v = eyePointData.getEye().nSub(vertex);

        IVector k = n.copy().normalize().scalarMultiply(l.scalarProduct(n.copy().normalize()));
        IVector p = k.nSub(l);
        IVector r = l.add(p.scalarMultiply(2)).normalize();

        return r.cosine(v);
    }
}
