import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.Animator;
import structures.linalg.util.IncompatibleOperandException;
import structures.linalg.util.ReadOnlyException;
import structures.linalg.vector.IVector;
import structures.linalg.vector.Vector;
import structures.objectStructures.Object3D;
import structures.objectStructures.Point3D;
import structures.util.EyePointData;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class Main {


    static{
        GLProfile.initSingleton();
    }

    private enum ShadingMode{
        FLAT,
        GOURAUD
    }

    static Object3D object3D;
    static boolean z = true;
    static ShadingMode shadingMode = ShadingMode.FLAT;

    static final EyePointData eyePointData = new EyePointData(40, 40, 40);
    static final BSpline spline = new BSpline("data/spline_data.txt");
    static final IVector s = new Vector(new double[]{0, 0, 1});
    static final long TIME_INTERVAL = 10000;
    static final int POINTS_PER_SEGMENT = 120;

    private static long startTimestamp;

    private static void resetTimestamp(){
        startTimestamp = System.currentTimeMillis();
    }

    public static void main(String[] args) {
        spline.sampleSplinePoints(POINTS_PER_SEGMENT);

        String path = args[0];
        object3D = new Object3D(path);

        System.out.println("Konstantno sjencanje");

        SwingUtilities.invokeLater(() -> {
            final GLProfile glProfile = GLProfile.getDefault();
            final GLCapabilities glCapabilities = new GLCapabilities(glProfile);
            final GLCanvas glCanvas = new GLCanvas(glCapabilities);

            Animator animator = new Animator();
            animator.add(glCanvas);
            animator.start();

            glCanvas.addKeyListener(eyePointData.getKeyAdapter());

            glCanvas.addKeyListener(new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent keyEvent) {
                    keyEvent.consume();
                    if(keyEvent.getKeyCode() == KeyEvent.VK_Z){
                        z = !z;
                        System.out.println("Z-spremnik je " + (z ? "ukljucen" : "iskljucen"));
                    } else if (keyEvent.getKeyCode() == KeyEvent.VK_K){
                        shadingMode = ShadingMode.FLAT;
                        System.out.println("Konstanto sjencanje");
                    } else if (keyEvent.getKeyCode() == KeyEvent.VK_G){
                        shadingMode = ShadingMode.GOURAUD;
                        System.out.println("Gouraudovo sjencanje");
                    }
                    glCanvas.display();
                }
            });

            glCanvas.addGLEventListener(new GLEventListener() {
                @Override
                public void init(GLAutoDrawable glAutoDrawable) {
                    eyePointData.setGlCanvas(glCanvas);
                }

                @Override
                public void dispose(GLAutoDrawable glAutoDrawable) {

                }

                @Override
                public void display(GLAutoDrawable glAutoDrawable) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();
                    gl2.glClearColor(1, 1, 1, 0);
                    gl2.glLineWidth(3.f);
                    if(z){
                        gl2.glEnable(GL2.GL_DEPTH_TEST);
                    } else {
                        gl2.glDisable(GL2.GL_DEPTH_TEST);
                    }
                    gl2.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
                    gl2.glLoadIdentity();

                    GLU glu = new GLU();
                    IVector eye = eyePointData.calculateEye();
                    glu.gluLookAt(eye.get(0), eye.get(1), eye.get(2), 0f, 0f, 25f, 0f, 0f, 1f);

                    List<IVector> points = spline.getPoints();
                    List<IVector> tangents = spline.getTangents();

                    int noPoints = points.size();

                    long currentTimestamp = System.currentTimeMillis();
                    int index = (int) ((double) (currentTimestamp - startTimestamp) / (double) TIME_INTERVAL * noPoints);
                    if(index >= noPoints){
                        resetTimestamp();
                        index = 0;
                    }
                    //draw the curve

                    gl2.glColor3f(0f, 0f, 0f);
                    gl2.glBegin(GL2.GL_LINE_STRIP);
                    for(IVector point: points){
                        gl2.glVertex3d(point.get(0), point.get(1), point.get(2));
                    }
                    gl2.glEnd();

                    gl2.glPolygonMode(GL2.GL_FRONT, GL2.GL_FILL);
                    gl2.glEnable(GL.GL_CULL_FACE);
                    gl2.glCullFace(GL2.GL_BACK);

                    gl2.glLightModelfv(GL2.GL_LIGHT_MODEL_AMBIENT, new float[]{0.0f, 0.0f, 0.0f, 1f}, 0);

                    gl2.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, new float[]{30f, 30f, 60f, 1f}, 0);
                    gl2.glLightfv(GL2.GL_LIGHT0, GL2.GL_AMBIENT, new float[]{0.2f, 0.6f, 0.2f, 1f}, 0);
                    gl2.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, new float[]{0.8f, 0.8f, 0f, 1f}, 0);
                    gl2.glLightfv(GL2.GL_LIGHT0, GL2.GL_SPECULAR, new float[]{0f, 0f, 0f, 1f}, 0);
                    gl2.glEnable(GL2.GL_LIGHT0);

                    gl2.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT, new float[]{1f, 1f, 1f, 1f}, 0);
                    gl2.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, new float[]{1f, 1f, 1f, 1f}, 0);
                    gl2.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, new float[]{0.01f, 0.01f, 0.01f, 1f}, 0);
                    gl2.glMaterialf(GL2.GL_FRONT, GL2.GL_SHININESS, 96f);

                    try {
                        IVector translationVector = points.get(index);
                        IVector e = tangents.get(index).normalize();
                        IVector rotationAxis = s.nVectorProduct(e);
                        double angle = Math.toDegrees(Math.acos(s.scalarProduct(e) / (s.norm() * e.norm())));



                        //draw the curve tangent
                        gl2.glLineWidth(2f);
                        gl2.glBegin(GL2.GL_LINE_STRIP);
                        gl2.glColor3d(1., 0., 0.);
                        gl2.glVertex3d(translationVector.get(0), translationVector.get(1), translationVector.get(2));
                        gl2.glVertex3d(translationVector.get(0) + 10 * e.get(0), translationVector.get(1) + 10 * e.get(1), translationVector.get(2) + 10 * e.get(2));
                        gl2.glEnd();


                        gl2.glPushMatrix();
                        gl2.glTranslated(translationVector.get(0), translationVector.get(1), translationVector.get(2));
                        gl2.glRotated(angle, rotationAxis.get(0), rotationAxis.get(1), rotationAxis.get(2));
                        gl2.glScaled(4, 4, 4);
                        gl2.glColor3d(0, 1, 0.6);
                        gl2.glEnable(GL2.GL_LIGHTING);
                        renderScene(gl2);
                        gl2.glDisable(GL2.GL_LIGHTING);

                        gl2.glPopMatrix();
                    } catch (ReadOnlyException | IncompatibleOperandException ex) {
                        ex.printStackTrace();
                    }

                }

                @Override
                public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();
                    gl2.glMatrixMode(GL2.GL_PROJECTION);
                    gl2.glLoadIdentity();
                    gl2.glFrustum(-0.5, 0.5, -0.5, 0.5, 1, 100);

                    gl2.glMatrixMode(GL2.GL_MODELVIEW);
                    gl2.glViewport(0, 0, width, height);
                }
            });

            final JFrame jframe = new JFrame("B-Spline");
            jframe.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            jframe.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent windowEvent) {
                    jframe.dispose();
                    System.exit(0);
                }
            });
            jframe.getContentPane().add(glCanvas, BorderLayout.CENTER);
            jframe.setSize(960, 960);
            jframe.setLocationRelativeTo(null);
            jframe.setVisible(true);
            glCanvas.requestFocusInWindow();
            glCanvas.display();
        });
    }

    private static void renderScene(GL2 gl2) throws ReadOnlyException {
        if(shadingMode == ShadingMode.FLAT){
            gl2.glShadeModel(GL2.GL_FLAT);
            drawFlat(gl2);
        }
        else if (shadingMode == ShadingMode.GOURAUD) {
            gl2.glShadeModel(GL2.GL_SMOOTH);
            drawGouraud(gl2);
        }
    }

    private static void drawFlat(GL2 gl2) throws ReadOnlyException {

        int size = object3D.getNoPolygons();
        for (int i = 0; i < size; i++){
            List<Point3D> polygon = object3D.getPolygon(i);
            gl2.glBegin(GL2.GL_POLYGON);
            for(int j = 0; j < 3; j++){
                IVector polygonNormal = object3D.getPolygonNormal(i);
                gl2.glNormal3d(polygonNormal.get(0), polygonNormal.get(1), polygonNormal.get(2));
                gl2.glVertex3d(polygon.get(j).getX(), polygon.get(j).getY(), polygon.get(j).getZ());
            }
            gl2.glEnd();
        }
    }

    private static void drawGouraud(GL2 gl2){

        int size = object3D.getNoPolygons();
        for(int i = 0; i < size; i++){
            List<Point3D> polygon = object3D.getPolygon(i);
            gl2.glBegin(GL2.GL_POLYGON);
            for(int j = 0; j < 3; j++){
                Point3D vertex = polygon.get(j);
                IVector vertexNormal = vertex.getNormal();
                gl2.glNormal3d(vertexNormal.get(0), vertexNormal.get(1), vertexNormal.get(2));
                gl2.glVertex3d(vertex.getX(), vertex.getY(), vertex.getZ());
            }
            gl2.glEnd();
        }
    }
}
