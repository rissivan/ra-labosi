import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import structures.linalg.util.ReadOnlyException;
import structures.linalg.vector.IVector;
import structures.objectStructures.Object3D;
import structures.objectStructures.Point3D;
import structures.util.EyePointData;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class Main_9_1 {


    static{
        GLProfile.initSingleton();
    }

    private enum ShadingMode{
        FLAT,
        GOURAUD
    };

    static Object3D object3D;
    static boolean z = false;
    static ShadingMode shadingMode = ShadingMode.FLAT;

    static final EyePointData eyePointData = new EyePointData(3, 4, 1);

    public static void main(String[] args) {

        String path = args[0];
        object3D = new Object3D(path);

        SwingUtilities.invokeLater(() -> {
            final GLProfile glProfile = GLProfile.getDefault();
            final GLCapabilities glCapabilities = new GLCapabilities(glProfile);
            final GLCanvas glCanvas = new GLCanvas(glCapabilities);

            glCanvas.addKeyListener(eyePointData.getKeyAdapter());

            glCanvas.addKeyListener(new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent keyEvent) {
                    keyEvent.consume();
                    if(keyEvent.getKeyCode() == KeyEvent.VK_Z){
                        z = !z;
                        System.out.println("Z-spremnik je " + (z ? "ukljucen" : "iskljucen"));
                    } else if (keyEvent.getKeyCode() == KeyEvent.VK_K){
                        shadingMode = ShadingMode.FLAT;
                    } else if (keyEvent.getKeyCode() == KeyEvent.VK_G){
                        shadingMode = ShadingMode.GOURAUD;
                    }
                    glCanvas.display();
                }
            });

            glCanvas.addGLEventListener(new GLEventListener() {
                @Override
                public void init(GLAutoDrawable glAutoDrawable) {
                    eyePointData.setGlCanvas(glCanvas);
                }

                @Override
                public void dispose(GLAutoDrawable glAutoDrawable) {

                }

                @Override
                public void display(GLAutoDrawable glAutoDrawable) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();
                    gl2.glClearColor(1, 1, 1, 0);
                    if(z){
                        gl2.glEnable(GL2.GL_DEPTH_TEST);
                    } else {
                        gl2.glDisable(GL2.GL_DEPTH_TEST);
                    }
                    gl2.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
                    gl2.glLoadIdentity();

                    GLU glu = new GLU();
                    IVector eye = eyePointData.calculateEye();
                    glu.gluLookAt(eye.get(0), eye.get(1), eye.get(2), 0, 0, 0, 0, 1, 0);

                    gl2.glPolygonMode(GL2.GL_FRONT, GL2.GL_FILL);
                    gl2.glEnable(GL.GL_CULL_FACE);
                    gl2.glCullFace(GL2.GL_BACK);


                    gl2.glEnable(GL2.GL_LIGHTING);

                    gl2.glLightModelfv(GL2.GL_LIGHT_MODEL_AMBIENT, new float[]{0.0f, 0.0f, 0.0f, 1f}, 0);

                    gl2.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, new float[]{4f, 5f, 3f, 1f}, 0);
                    gl2.glLightfv(GL2.GL_LIGHT0, GL2.GL_AMBIENT, new float[]{0.2f, 0.2f, 0.2f, 1f}, 0);
                    gl2.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, new float[]{0.8f, 0.8f, 0f, 1f}, 0);
                    gl2.glLightfv(GL2.GL_LIGHT0, GL2.GL_SPECULAR, new float[]{0f, 0f, 0f, 1f}, 0);
                    gl2.glEnable(GL2.GL_LIGHT0);

                    gl2.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT, new float[]{1f, 1f, 1f, 1f}, 0);
                    gl2.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, new float[]{1f, 1f, 1f, 1f}, 0);
                    gl2.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, new float[]{0.01f, 0.01f, 0.01f, 1f}, 0);
                    gl2.glMaterialf(GL2.GL_FRONT, GL2.GL_SHININESS, 96f);



                    try {
                        renderScene(gl2);
                    } catch (ReadOnlyException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
                    GL2 gl2 = glAutoDrawable.getGL().getGL2();
                    gl2.glMatrixMode(GL2.GL_PROJECTION);
                    gl2.glLoadIdentity();
                    gl2.glFrustum(-0.5, 0.5, -0.5, 0.5, 1, 100);

                    gl2.glMatrixMode(GL2.GL_MODELVIEW);
                    gl2.glViewport(0, 0, width, height);
                }
            });

            final JFrame jframe = new JFrame("Zadatak 9.1");
            jframe.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            jframe.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent windowEvent) {
                    jframe.dispose();
                    System.exit(0);
                }
            });
            jframe.getContentPane().add(glCanvas, BorderLayout.CENTER);
            jframe.setSize(640,480);
            jframe.setLocationRelativeTo(null);
            jframe.setVisible(true);
            glCanvas.requestFocusInWindow();
        });
    }

    private static void renderScene(GL2 gl2) throws ReadOnlyException {
        if(shadingMode == ShadingMode.FLAT){
            gl2.glShadeModel(GL2.GL_FLAT);
            drawFlat(gl2);
        }
        else if (shadingMode == ShadingMode.GOURAUD) {
            gl2.glShadeModel(GL2.GL_SMOOTH);
            drawGouraud(gl2);
        }
    }

    private static void drawFlat(GL2 gl2) throws ReadOnlyException {

        int size = object3D.getNoPolygons();
        for (int i = 0; i < size; i++){
            List<Point3D> polygon = object3D.getPolygon(i);
            gl2.glBegin(GL2.GL_POLYGON);
            for(int j = 0; j < 3; j++){
                IVector polygonNormal = object3D.getPolygonNormal(i);
                gl2.glNormal3d(polygonNormal.get(0), polygonNormal.get(1), polygonNormal.get(2));
                gl2.glVertex3d(polygon.get(j).getX(), polygon.get(j).getY(), polygon.get(j).getZ());
            }
            gl2.glEnd();
        }
    }

    private static void drawGouraud(GL2 gl2){

        int size = object3D.getNoPolygons();
        for(int i = 0; i < size; i++){
            List<Point3D> polygon = object3D.getPolygon(i);
            gl2.glBegin(GL2.GL_POLYGON);
            for(int j = 0; j < 3; j++){
                Point3D vertex = polygon.get(j);
                IVector vertexNormal = vertex.getNormal();
                gl2.glNormal3d(vertexNormal.get(0), vertexNormal.get(1), vertexNormal.get(2));
                gl2.glVertex3d(vertex.getX(), vertex.getY(), vertex.getZ());
            }
            gl2.glEnd();
        }
    }
}
