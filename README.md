Laboratorijske vježbe iz predmeta Računalna animacija, ak. god. 2021./2022.

1. laboratorijska vježba, napisana u Javi, nalazi se u direktoriju Lab1.
2. laboratorijska vježba, napisana u C++-u, nalazi se u direktoriju Lab2cpp.
3. (samostalna) laboratorijska vježba, napisana u C++-u, nalazi se u direktoriju Projekt. Upute za pokretanje te opis korištenog algoritma nalaze se u dokumentaciji (datoteci 2022IvanRissi.pdf).

